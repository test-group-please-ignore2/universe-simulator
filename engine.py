# py libraries
from dataclasses import dataclass, field
import random

from PyQt5.QtCore import QObject, pyqtSignal
from PyQt5.QtWidgets import QMdiArea, QMenuBar

# my libraries
from celestials import Constellation, Galaxy, Planet, Star
from encounters import new_encounter
from graphics import GalaxyMap, SystemMap
from widgets import ConstList, LocationInfo, PlanetInfo, PlanetList, StarInfo, StarList
from windows import NewGameWindow, OkPopUp, ShipBattleWindow, ShipTravelWindow


class GameSignals(QObject):
    ftl_ok_signal = pyqtSignal(bool)
    stl_ok_signal = pyqtSignal(bool)


@dataclass
class GameWidgets(QObject):
    def __post_init__(self):
        super().__init__()
        # initialize windows
        self.constList = ConstList()
        self.starList = StarList()
        self.starInfo = StarInfo()
        self.planetList = PlanetList()
        self.planetInfo = PlanetInfo()
        self.galaxyMap = GalaxyMap(None)
        self.systemMap = SystemMap(None)
        self.locationInfo = LocationInfo()


@dataclass
class GameWindows():
    mainArea: QMdiArea
    widgets: GameWidgets

    def __post_init__(self):
        # init windows
        self.newGame = NewGameWindow()
        self.shipTravel = ShipTravelWindow(self.widgets)
        self.shipBattle = ShipBattleWindow()
        # add windows
        self.mainArea.addSubWindow(self.newGame)
        self.mainArea.addSubWindow(self.shipTravel)
        self.mainArea.addSubWindow(self.shipBattle)


@dataclass
class GameState():
    galaxy: Galaxy
    widgets: GameWidgets
    windows: GameWindows

    def __post_init__(self):
        # celestial lists
        self.consts = self.galaxy.get_consts()
        self.stars = self.galaxy.get_stars()

        # locations
        self.current_const = random.choice(
            [const for const in self.consts]
        )

        self.current_star = random.choice(
            [star for star in self.current_const.stars]
        )

        self.current_planet = random.choice(
            [planet for planet in self.current_star.orbitals]
        )

        # ships
        self.player_ship = None
        self.target_ship = None

        # connections and signals
        self.signals = GameSignals()
        self.set_connections()
        self.set_signals()

        # update windows and widgets
        self.set_galaxy()

    """
        Game Initialization
    """

    def set_connections(self):
        # list selection connections
        self.widgets.constList.const_selected_signal.connect(self.set_selected_const)
        self.widgets.starList.star_selected_signal.connect(self.set_selected_star)
        self.widgets.planetList.planet_selected_signal.connect(self.set_selected_planet)
        # zoom connections
        self.windows.shipTravel.zoomToConst.clicked.connect(self.zoom_to_current_const)
        self.windows.shipTravel.zoomToStar.clicked.connect(self.zoom_to_current_star)
        self.windows.shipTravel.zoomToSystem.clicked.connect(self.zoom_to_current_system)
        self.windows.shipTravel.zoomToPlanet.clicked.connect(self.zoom_to_current_planet)
        # travel connections
        self.windows.shipTravel.ftlButton.clicked.connect(self.ftl_travel)
        self.windows.shipTravel.stlButton.clicked.connect(self.stl_travel)

    def set_signals(self):
        self.signals.ftl_ok_signal.connect(self.ftl_enable)
        self.signals.stl_ok_signal.connect(self.stl_enable)

    """
        Set Current Functions
    """

    def set_galaxy(self):
        self.widgets.galaxyMap.set_galaxy(self.galaxy)
        self.widgets.constList.set_galaxy(self.galaxy)
        self.set_current_const(self.current_const)
        self.set_current_star(self.current_star)
        self.set_current_planet(self.current_planet)

    def set_current_const(self, const: Constellation):
        # update current
        self.current_const = const
        # update widgets

    def set_current_star(self, star: Star):
        # update current
        self.current_star = star
        # update widgets
        self.widgets.systemMap.set_star(star)
        self.widgets.systemMap.update()
        self.widgets.planetList.set_star(star)
        self.widgets.planetList.update()
        self.widgets.locationInfo.update_info(star, None)


    def set_current_planet(self, planet: Planet):
        # update current
        self.current_planet = planet
        # update widgets
        self.widgets.locationInfo.update_info(self.current_star, planet)

    """
        Set Selected Functions
    """

    def set_selected_const(self, const: Constellation):
        # update widgets
        self.widgets.galaxyMap.set_const(const)
        self.widgets.starList.set_const(const)

    def set_selected_star(self, star: Star):
        # update widgets
        self.widgets.galaxyMap.set_star(star)
        self.widgets.starInfo.update_info(star)
        self.signals.ftl_ok_signal.emit(True)

    def set_selected_planet(self, planet: Planet):
        # update widgets
        self.widgets.systemMap.set_planet(planet)
        self.widgets.planetInfo.update_info(planet)
        self.signals.stl_ok_signal.emit(True)

    """
        Map Functions
    """

    def zoom_to_current_const(self):
        self.widgets.galaxyMap.set_const(self.current_const)

    def zoom_to_current_star(self):
        self.widgets.galaxyMap.set_star(self.current_star)

    def zoom_to_current_system(self):
        self.widgets.systemMap.set_star(self.current_star)

    def zoom_to_current_planet(self):
        self.widgets.systemMap.set_planet(self.current_planet)

    """
        Travel Functions
    """

    def ftl_enable(self):
        self.windows.shipTravel.ftlButton.setEnabled(True)

    def ftl_travel(self):
        const = self.windows.shipTravel.get_selected_const()
        star = self.windows.shipTravel.get_selected_star()

        star = self.galaxy.get_star(star)
        if star is not None:
            self.set_current_const(const)
            self.set_current_star(star)

        self.windows.shipTravel.ftlButton.setEnabled(False)


    def stl_enable(self) -> None:
        self.windows.shipTravel.stlButton.setEnabled(True)

    def stl_travel(self) -> None:

        name = self.windows.shipTravel.get_selected_planet()

        dest = None
        for planet in self.current_star.orbitals: 
            if planet.name == name:
                dest = planet

        if dest is not None:
            self.set_current_planet(dest)

        self.windows.shipTravel.stlButton.setEnabled(False)

        e = new_encounter(self.current_star, self.current_planet)
        popup = OkPopUp(e.title, e.text, parent=self.windows.mainArea)
        popup.show()