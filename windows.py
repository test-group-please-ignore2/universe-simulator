# ui libraries
from datetime import datetime
from PyQt5.QtWidgets import QComboBox, QDialog, QFormLayout, QHBoxLayout, QLabel, QMdiSubWindow, QOpenGLWidget, QProgressBar, QPushButton, QSizePolicy, QSpinBox, QTextBrowser, QVBoxLayout, QWidget
from PyQt5.Qt import Qt
from qtpy.QtCore import QTimer

# my libraries
from graphics import GalaxyMap, SystemMap


"""
    Settings Windows
"""


class NewGameWindow(QMdiSubWindow):
    def __init__(self):
        super().__init__()
        # window stuff
        window_flags = Qt.Window | Qt.CustomizeWindowHint | Qt.WindowTitleHint
        self.setWindowFlags(window_flags)

        # create content widget and layout
        content_widget = QWidget()
        content_layout = QHBoxLayout()
        content_widget.setLayout(content_layout)

        # create form layout and widgets on the left side
        form_layout = QFormLayout()

        # generation form options
        self.maxConst = QSpinBox()
        self.maxConst.setValue(50)
        form_layout.addRow(QLabel("Constellations to Generate:"), self.maxConst)

        self.maxStars = QSpinBox()
        self.maxStars.setValue(10)
        form_layout.addRow(QLabel("Max Stars per Constellation:"), self.maxStars)

        self.maxPlanets = QSpinBox()
        self.maxPlanets.setValue(10)
        form_layout.addRow(QLabel("Max Planets per System:"), self.maxPlanets)

        self.maxMoons = QSpinBox()
        self.maxMoons.setValue(3)
        form_layout.addRow(QLabel("Max Moons per Planet:"), self.maxMoons)

        self.mapSize = QSpinBox()
        self.mapSize.setMaximum(99999)
        self.mapSize.setValue(1000)
        form_layout.addRow(QLabel("Map Size:"), self.mapSize)

        self.generateGalaxy = QPushButton("Generate Galaxy")
        form_layout.addRow(self.generateGalaxy)

        # add form layout to the content layout
        content_layout.addLayout(form_layout)

        # create QTextBrowser on the right side
        self.textConsole = QTextBrowser()
        content_layout.addWidget(self.textConsole)

        # set content widget for QMdiSubWindow
        self.setWidget(content_widget)

    def open(self):
        self.showMaximized()

    def write(self, args):
        """
        This function writes the contents of msg_sig to the console when it is
        triggered.

        Parameters:
        -----------
            None

        Returns:
        --------
            None

        """

        # set timestamp
        timestamp = datetime.now().strftime("%Y/%m/%d %H:%M:%S")
        text = ""

        # print the text
        for arg in args:
            text = text + arg

        msg = timestamp + " >> " + text
        self.textConsole.append(msg)


"""
    Ship Windows
"""


class ShipTravelWindow(QMdiSubWindow):
    def __init__(self, widgets):
        super().__init__()
        # ADD THE FPS
        self.map_update_timer = QTimer()
        self.map_update_timer.timeout.connect(self.update_maps)
        self.map_update_timer.start(6)

        # window stuff
        window_flags = Qt.Window | Qt.CustomizeWindowHint | Qt.WindowTitleHint
        self.setWindowFlags(window_flags)

        self.setWindowFlags(window_flags)
        self.galaxyMap = widgets.galaxyMap
        self.systemMap = widgets.systemMap
        self.constList = widgets.constList
        self.starList = widgets.starList
        self.starInfo = widgets.starInfo
        self.planetList = widgets.planetList
        self.planetInfo = widgets.planetInfo
        self.locationInfo = widgets.locationInfo

        # initialise ui parts
        contentWidget = QWidget()
        contentLayout = QHBoxLayout()

        # initialise travel options
        travelWidget = QWidget()
        travelLayout = QVBoxLayout()
        travelWidget.setLayout(travelLayout)

        # initialise info layout
        self.infoWidget = QWidget()
        infoLayout = QVBoxLayout()
        self.infoWidget.setLayout(infoLayout)

        # travel mode dropdown
        self.travelMode = QComboBox()
        self.travelMode.addItems(['STL Drive', 'FTL Drive'])  # Put "STL Drive" first

        # ftl travel stuff
        self.ftlButton = QPushButton('FTL to Selected')
        self.ftlButton.setEnabled(False)

        # stl travel stuff
        self.stlButton = QPushButton('STL to Selected')
        self.stlButton.setEnabled(False)

        # zoom to const / star buttons
        self.zoomToSystem = QPushButton('Show System')
        self.zoomToPlanet = QPushButton('Current Planet')
        self.zoomToConst = QPushButton('Show Constellation')
        self.zoomToStar = QPushButton('Current Star')

        # left side
        travelLayout.addWidget(QLabel('Select Mode:'))
        travelLayout.addWidget(self.travelMode)
        travelLayout.addWidget(self.stlButton)
        travelLayout.addWidget(self.ftlButton)
        travelLayout.addWidget(self.planetList)
        travelLayout.addWidget(self.constList)
        travelLayout.addWidget(self.starList)

        # right side
        infoLayout.addWidget(self.zoomToSystem)
        infoLayout.addWidget(self.zoomToPlanet)
        infoLayout.addWidget(self.zoomToConst)
        infoLayout.addWidget(self.zoomToStar)
        infoLayout.addWidget(self.locationInfo)
        infoLayout.addWidget(self.starInfo)
        infoLayout.addWidget(self.planetInfo)

        # Hide star list and info widgets by default (STL mode)
        self.constList.hide()
        self.starList.hide()
        self.ftlButton.hide()
        self.starInfo.hide()
        self.zoomToConst.hide()
        self.zoomToStar.hide()

        # set up the content widget and layout
        contentWidget.setLayout(contentLayout)
        contentLayout.addWidget(travelWidget)
        contentLayout.addWidget(self.systemMap)  # Display the SystemMap by default
        contentLayout.addWidget(self.infoWidget)
        self.galaxyMap.hide()  # Hide the GalaxyMap

        self.setWidget(contentWidget)
        self.setSizePolicy(
            QSizePolicy.Expanding,  # horizontal
            QSizePolicy.Preferred   # vertical
        )

        # signals
        self.travelMode.currentIndexChanged.connect(self.change_travel_mode)

    def update_maps(self):
        self.systemMap.update()
        self.galaxyMap.update()

    def open(self):
        self.showMaximized()

    def get_selected_const(self):

        return self.constList.consts.currentItem().text()

    def get_selected_star(self):

        return self.starList.stars.currentItem().text()

    def get_selected_planet(self):

        return self.planetList.planets.currentItem().text()


    def change_travel_mode(self):
        current_mode = self.travelMode.currentText()
        contentLayout = self.widget().layout()

        if current_mode == "FTL Drive":
            contentLayout.removeWidget(self.systemMap)
            contentLayout.removeWidget(self.infoWidget)
            self.systemMap.hide()
            contentLayout.addWidget(self.galaxyMap)
            contentLayout.addWidget(self.infoWidget)
            self.galaxyMap.show()

            # Show star list and info widgets
            # lists
            self.constList.show()
            self.starList.show()
            # buttons
            self.zoomToConst.show()
            self.zoomToStar.show()
            self.ftlButton.show()
            # info
            self.starInfo.show()

            # Hide planet list and info widgets
            # lists
            self.planetList.hide()
            # buttons
            self.zoomToSystem.hide()
            self.zoomToPlanet.hide()
            self.stlButton.hide()
            # info
            self.planetInfo.hide()

        elif current_mode == "STL Drive":
            contentLayout.removeWidget(self.galaxyMap)
            contentLayout.removeWidget(self.infoWidget)
            self.galaxyMap.hide()
            contentLayout.addWidget(self.systemMap)
            contentLayout.addWidget(self.infoWidget)
            self.systemMap.show()
            self.systemMap.update()

            # Hide star list and info widgets
            # lists
            self.constList.hide()
            self.starList.hide()
            # buttons
            self.zoomToConst.hide()
            self.zoomToStar.hide()
            self.ftlButton.hide()
            # info
            self.starInfo.hide()

            # Show planet list and info widgets
            # lists
            self.planetList.show()
            # buttons
            self.zoomToSystem.show()
            self.zoomToPlanet.show()
            self.stlButton.show()
            # info
            self.planetInfo.show()


class ShipBattleWindow(QMdiSubWindow):
    def __init__(self):
        super().__init__()
        # window stuff
        window_flags = Qt.Window | Qt.CustomizeWindowHint | Qt.WindowTitleHint
        self.setWindowFlags(window_flags)

        # initialise ships
        # TODO: add ships
        self.playerName = "Player"
        self.targetName = "Target"

        # initialise ui parts
        contentWidget = QWidget()
        contentLayout = QHBoxLayout()

        """
            Left Side
        """

        # init left ui parts
        playerWidget = QWidget()
        playerLayout = QVBoxLayout()
        playerWidget.setLayout(playerLayout)

        # set up player HP widget
        playerHPLayout = QFormLayout()
        playerHPWidget = QWidget()
        playerHPWidget.setLayout(playerHPLayout)

        # ship name
        playerHPLayout.addRow(
            QLabel('Ship:'), QLabel(self.playerName)
        )

        # armor values
        self.playerArmorDisplay = QProgressBar()
        self.playerArmorDisplay.setValue(100)
        playerHPLayout.addRow(
            QLabel('Armor:'), self.playerArmorDisplay
        )

        # hull values
        self.playerHullDisplay = QProgressBar()
        self.playerHullDisplay.setValue(100)
        playerHPLayout.addRow(
            QLabel('Hull:'), self.playerHullDisplay
        )

        # structure values
        self.playerStructDisplay = QProgressBar()
        self.playerStructDisplay.setValue(100)
        playerHPLayout.addRow(
            QLabel('Structure:'), self.playerStructDisplay
        )

        playerLayout.addWidget(playerHPWidget)

        """
            Centre Side
        """

        # init centre ui parts
        self.battleLog = QTextBrowser()

        """
            Right Side
        """

        # init right ui parts
        targetWidget = QWidget()
        targetLayout = QVBoxLayout()
        targetWidget.setLayout(targetLayout)

        # set up target HP widget
        targetHPLayout = QFormLayout()
        targetHPWidget = QWidget()
        targetHPWidget.setLayout(targetHPLayout)

        # ship name
        targetHPLayout.addRow(
            QLabel('Ship:'), QLabel(self.targetName)
        )

        # armor values
        self.targetArmorDisplay = QProgressBar()
        self.targetArmorDisplay.setValue(100)
        targetHPLayout.addRow(
            QLabel('Armor:'), self.targetArmorDisplay
        )

        # hull values
        self.targetHullDisplay = QProgressBar()
        self.targetHullDisplay.setValue(100)
        targetHPLayout.addRow(
            QLabel('Hull:'), self.targetHullDisplay
        )

        # structure values
        self.targetStructDisplay = QProgressBar()
        self.targetStructDisplay.setValue(100)
        targetHPLayout.addRow(
            QLabel('Structure:'), self.targetStructDisplay
        )

        targetLayout.addWidget(targetHPWidget)


        """
            Make The Window
        """

        # set layout and add widgets
        contentWidget.setLayout(contentLayout)
        contentLayout.addWidget(playerWidget)
        contentLayout.addWidget(self.battleLog)
        contentLayout.addWidget(targetWidget)

        self.setWidget(contentWidget)
        self.setSizePolicy(
            QSizePolicy.Expanding,  # horizontal
            QSizePolicy.Preferred   # vertical
        )

    def open(self):
        self.showMaximized()

    def clear_log(self):
        self.battleLog.clear

    def write(self, *messages):

        [self.battleLog.append(msg) for msg in messages]


class OkPopUp(QDialog):
    def __init__(self, title, text, parent=None):
        super().__init__(parent)

        # set window properties
        self.setGeometry(0, 0, 200, 200)
        self.setWindowFlags(self.windowFlags() | Qt.WindowStaysOnTopHint)
        self.setWindowTitle(title)

        # create widgets
        label = QLabel(text)
        label.setWordWrap(True)
        ok_button = QPushButton("OK")
        ok_button.clicked.connect(self.accept)

        # layout widgets
        vbox = QVBoxLayout()
        vbox.addWidget(label)
        hbox = QHBoxLayout()
        hbox.addStretch(1)
        hbox.addWidget(ok_button)
        hbox.addStretch(1)
        vbox.addStretch(1)
        vbox.addLayout(hbox)
        self.setLayout(vbox)

        # center on QMdiArea
        self.center_on_area()

    def center_on_area(self):
        qmdiarea = self.parent()
        qmdiarea_geometry = qmdiarea.geometry()  # type: ignore
        popup_geometry = self.geometry()
        global_top_left = qmdiarea.mapToGlobal(  # type: ignore
            qmdiarea_geometry.topLeft()
        )

        x = global_top_left.x() + (qmdiarea_geometry.width() - popup_geometry.width()) // 2
        y = global_top_left.y() + (qmdiarea_geometry.height() - popup_geometry.height()) // 2

        self.move(x, y)

    def accept(self):
        self.close()

