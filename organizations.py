from dataclasses import dataclass, field


@dataclass
class Organization:
    """
    This is the base class for an organization.
    """

    name:           str = ""
    headquarters:   str = ""
    holdings:       str = ""
    affiliation:    str = ""
    subordinates:   str = ""
    attr:           dict = field(default_factory=dict)
    read_attr:      dict = field(default_factory=dict)

    def government_organization(self):
        """
        This function creates a new
        """


@dataclass
class Government(Organization):
    """
    This is the base class for a government.
    """

    def generate_government(self):
        """
        This function provisions the organization with government-like
        attributes.
        """


@dataclass
class Corporation(Organization):
    """
    This is the base class for a corporation.
    """

    def generate_corporation(self):
        """
        This function provisions the organization with corporation-like
        attributes.
        """
