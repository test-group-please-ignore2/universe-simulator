import os

# path variables and resources
path = os.path.dirname(os.path.abspath(__file__)) + "/lists/"

terrestrial_types = path + "terrestrial_types.csv"
atmosphere_types = path + "atmosphere_types.csv"
biome_types = path + "biome_types.csv"
gaseous_types = path + "gaseous_types.csv"
moon_types = path + "moon_types.csv"
star_types = path + "star_types.csv"
station_types = path + "station_types.csv"
system_names = path + "system_names.csv"
