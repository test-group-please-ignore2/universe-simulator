# py libraries
import pandas as pd
import random

# my libraries
from foronoi import BoundingBox, Voronoi
from celestials import Constellation, Galaxy, Star, Planet
from graphics import math
from maths import PointInPolygon, PoissonDisc
from misc import csv_to_list, khaire

# u libraries
from PyQt5.QtCore import QThread, pyqtSignal

# lists
lists = "lists/"
data = "data/"
db_path = "galaxy.db"


class GenerateGalaxy(QThread):
    """
    New galaxy generation loop, now puts the stuff into a database. Will decide
    whether this is required or not later.

    Parameters:
    -----------
        max_consts : int
            Maximum number of constellations to generate.

        max_stars : int
            Maximum number of stars per constellation to generate.

        max_planets : int
            Maximum number of planets per star to generate.

        max_moons : int
            Maximum number of moons per planet to generate.

        map_size : int
            Length of the sides of the galaxy map.

    """

    random.seed()

    msg_sig = pyqtSignal(str)
    prog_sig = pyqtSignal(int, int)
    output_sig = pyqtSignal(object)

    def __init__(self, max_consts: int,
                 max_stars: int,
                 max_planets: int,
                 max_moons: int,
                 map_size: int):
        QThread.__init__(self)
        self.max_consts = max_consts
        self.max_stars = max_stars
        self.max_planets = max_planets
        self.max_moons = max_moons
        self.map_size = map_size

    def __del__(self):
        self.wait()

    def run(self):
        galaxy = self.generate_galaxy()
        self.output_sig.emit(galaxy)
        self.msg_sig.emit("Done.")

    def generate_galaxy(self):
        """
        This is the main galaxy generation loop. It also creates the database
        used to store the galaxy and all it's components.

        Parameters:
        -----------
            None

        Returns:
        --------
            galaxy : list
                A list of constellations.

        """
        # generate constellations
        consts = self.generate_constellations()

        # generate constellations
        for const in consts:
            # generate stars and get list of star names
            self.generate_stars(const)

            # execute query to put constellations in database
            self.msg_sig.emit(f"Generated constellation: {const.name}")

            for star in const.stars:
                # generate planets and get list of planet names
                self.generate_planets(star)

                # generate planets
                for planet in star.orbitals:
                    # generate moons and get list of moon names
                    self.generate_moons(planet)

        galaxy = Galaxy(consts)

        return galaxy

    def generate_constellations(self):
        """
        This function generates the constellations using voronoi geometry
        and assigns them names.
        """

        # get name list and set number of constellations to generate
        const_name_list = csv_to_list(lists, "system_names.csv")
        num_names = len(const_name_list)
        num_consts = self.max_consts

        # check if requested number of constellations exceeds number of names
        if num_consts > num_names:
            num_consts = num_names

        else:
            num_consts = self.max_consts
        # choose names for constellations

        # create empty constellation bucket
        const_pos = []

        # generate min and max bounds of galaxy
        side = self.map_size / 2
        polygon = BoundingBox(0, self.map_size, 0, self.map_size)
        r = int(self.map_size / 10)

        disc = PoissonDisc(self.map_size, self.map_size, r=r, k=5)
        const_pos = disc.sample()
        const_names = random.sample(const_name_list, len(const_pos))

        # create constellation borders using Voronoi geometry
        v = Voronoi(polygon)
        polygon.finish_edges(edges=v.edges)
        v = Voronoi(polygon)
        v.create_diagram(const_pos)

        # filter out empty sites
        const_sites = [site for site in v.sites]  # type: ignore

        # create bucket to hold constellations
        galaxy = []

        # main constellation generation loop
        for n in range(len(const_sites)):
            const_name = const_names[n]
            const_pos = {
                'x': const_sites[n].xy[0] - side,
                'y': const_sites[n].xy[1] - side
            }

            # get coordinates of constellation vertices
            const_points = [
                (point.xy[0] - side, point.xy[1] - side)
                for point in const_sites[n].vertices()
            ]

            # create constellation
            new_const = Constellation(const_name, const_pos,
                                      const_points)

            # append constellation to galaxy
            galaxy.append(new_const)

        return galaxy

    def generate_stars(self, const: Constellation):
        """
        This function generates stars inside a constellation.
        """

        star_df = pd.read_csv(lists + "star_types.csv")
        star_types = star_df['Class'].tolist()
        star_prob = star_df['Probability'].tolist()
        polygon = PointInPolygon(const.points, 5)
        area = math.pi * min(polygon.edge_distances) ** 2
        num_stars = int(min(random.randint(1, self.max_stars), area))

        star_count = 0
        while star_count <= num_stars:
            # new fast efficient way of getting a point in a polygon
            star_x, star_y = polygon.get_point_in_polygon()

            # assign star attributes
            star_greek = khaire(star_count + 1)
            star_name = f"{star_greek} {const.name}"
            star_type = random.choices(
                star_types,
                cum_weights=star_prob,
                k=1
            )
            pos = {
                'x': star_x,
                'y': star_y
            }
            new_star = Star(star_name, pos=pos)
            new_star.generate_star(star_type[0], star_df)
            const.stars.append(new_star)
            self.msg_sig.emit(f"Generated star: {new_star.name}")
            star_count += 1

    def generate_planets(self, star: Star):
        """
        This function generates planets orbiting a star.
        """

        num_planets = random.randint(1, self.max_planets)

        # set default value for last_orbit
        last_orbit = 0

        for n in range(num_planets):
            if n == 0:
                new_planet = star.planet_orbital(n, None)
                last_orbit = new_planet.attr['orbit']
            else:
                new_planet = star.planet_orbital(n, last_orbit)
                last_orbit = new_planet.attr['orbit']

            new_planet.generate_planet(
                star.attr['inner_HZ'], star.attr['outer_HZ'])

            if new_planet.attr['habitability'] >= 80:
                star.attr['has_habitable'] = True

            star.orbitals.append(new_planet)
            self.msg_sig.emit(f"Generated planet: {new_planet.name}")

    def generate_moons(self, planet: Planet):
        """
        This function generates moons orbiting a star.
        """

        num_moons = random.randint(1, self.max_moons)

        # set default value for last_orbit
        last_orbit = 0

        for n in range(num_moons):
            # set default last orbit
            last_orbit = 0

            # generate initial moons
            if n == 0:
                new_moon = planet.moon_orbital(n)
                last_orbit = new_moon.attr['orbit']

            # generate subsequent moons
            else:
                new_moon = planet.moon_orbital(n, last_orbit)
                last_orbit = new_moon.attr['orbit']

            new_moon.generate_moon()

            planet.orbitals.append(new_moon)
            self.msg_sig.emit(f"Generated moon: {new_moon.name}")
