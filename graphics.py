# py libraries
from typing import Union
import math
import re
import numpy as np

# ui libraries
import qdarkstyle
from PyQt5.QtWidgets import QOpenGLWidget, QSizePolicy
from PyQt5.QtGui import QSurfaceFormat, QColor

# gfx libraries
import OpenGL.GL as gl
import OpenGL.GLUT as glut

from celestials import Celestial, Constellation, Galaxy, Planet, Star

"""
    COLOUR STUFF
"""

# get background colour
stylesheet = qdarkstyle.load_stylesheet()
regex = re.compile(
    r"QWidget\s*{(?:.*?;)*\s*background-color:\s*(.+?);",
    re.IGNORECASE
)
match = regex.search(stylesheet)

# set background colour
if match:
    color = QColor(match.group(1))
    b_r = color.redF() * 0.5
    b_g = color.greenF() * 0.5
    b_b = color.blueF() * 0.5

else:
    b_r = 0.0
    b_g = 0.0
    b_b = 0.0

star_colours = {
    'Blue': [
        154 / 255, 175 / 255, 255 / 255, 255 / 255
    ],
    'Blue White': [
        201 / 255, 216 / 255, 255 / 255, 255 / 255
    ],
    'White': [
        248 / 255, 247 / 255, 253 / 255, 255 / 255
    ],
    'Yellow White': [
        252 / 255, 255 / 255, 212 / 255, 255 / 255
    ],
    'Yellow': [
        255 / 255, 243 / 255, 161 / 255, 255 / 255
    ],
    'Orange': [
        254 / 255, 210 / 255, 163 / 255, 255 / 255
    ],
    'Red': [
        255 / 255, 95 / 255, 83 / 255, 255 / 255
    ]
}


class Camera:
    def __init__(self, w, h):
        self.current_position = np.array([0, 0], dtype=np.float32)
        self.target_position = np.array([0, 0], dtype=np.float32)
        self.interpolation_factor = 0.0

        self.zoom = 1.0
        self.target_zoom = 1.0

        self.width = w
        self.height = h

    def lerp(self, start, end, t):
        return (1.0 - t) * start + t * end

    def get_pos(self):
        return self.current_position[0], self.current_position[1]

    def set_pos(self, x, y):
        self.current_position = np.array([x, y], dtype=np.float32)
        self.target_position = np.array([x, y], dtype=np.float32)

    def set_target_pos(self, x, y):
        self.target_position = np.array([x, y], dtype=np.float32)
        self.interpolation_factor = 0.0

    def update_pos(self):
        interpolation_speed = 0.001
        self.current_position = self.lerp(
            self.current_position,
            self.target_position,
            self.interpolation_factor
        )

        self.interpolation_factor = min(
            1.0, self.interpolation_factor + interpolation_speed
        )

    def set_target_zoom(self, zoom):
        self.target_zoom = zoom

    def update_zoom(self):
        interpolation_speed = 0.001
        self.zoom = self.lerp(
            self.zoom,
            self.target_zoom,
            self.interpolation_factor
        )

        self.interpolation_factor = min(
            1.0, self.interpolation_factor + interpolation_speed
        )

    def set_zoom(self, factor):
        self.target_zoom = self.zoom * factor
        self.zoom *= factor

    def apply(self):
        gl.glTranslatef(
            self.current_position[0] * self.zoom,
            self.current_position[1] * self.zoom,
            0
        )

        gl.glScalef(self.zoom, self.zoom, 1)

    """
        Advanced Zoom
    """

    def zoom_to_fit(self, obj, map_):
        if isinstance(obj, Constellation):
            self.zoom_to_fit_const(obj)

        elif isinstance(obj, Star) and type(map_) == GalaxyMap:
            self.zoom_to_fit_star(obj)

        elif isinstance(obj, Star) and type(map_) == SystemMap:
            self.zoom_to_fit_system(map_)

        elif isinstance(obj, Planet) and obj.orbitals:
            self.zoom_to_fit_planet(map_)

    def zoom_to_fit_const(self, const: Constellation):
        min_x = min(point[0] for point in const.points)
        max_x = max(point[0] for point in const.points)
        min_y = min(point[1] for point in const.points)
        max_y = max(point[1] for point in const.points)

        buffer = 50
        width = max_x - min_x + 2 * buffer
        height = max_y - min_y + 2 * buffer

        w, h = self.width, self.height

        screen_ratio = w / h

        if screen_ratio >= 1:
            target_zoom = self.width / width
        else:
            target_zoom = self.height / height

        self.target_zoom = target_zoom
        self.target_x = -(min_x + max_x) / 2
        self.target_y = -(min_y + max_y) / 2

        self.set_target_pos(self.target_x, self.target_y)

    def zoom_to_fit_star(self, star):
        self.target_x = -star.pos['x']
        self.target_y = -star.pos['y']

        self.set_target_pos(self.target_x, self.target_y)

    def zoom_to_fit_system(self, map_):
        max_orbit = map_.orbit_scale
        buffer = 50
        diameter = 2 * max_orbit

        min_dimension = min(self.width, self.height)

        self.target_zoom = (min_dimension - 2 * buffer) / diameter
        self.target_x = 0
        self.target_y = 0

        self.set_target_pos(self.target_x, self.target_y)

    def zoom_to_fit_planet(self, map_):
        max_orbit = max(planet.attr['orbit'] for planet in map_.star.orbitals)
        moon_orbit = max(moon.attr['orbit'] for moon in map_.planet.orbitals)
        scale = moon_orbit * map_.orbit_scale / max_orbit
        buffer = 50
        diameter = 2 * scale

        min_dimension = min(self.width, self.height)
        target_zoom = (min_dimension - 2 * buffer)  / diameter

        self.target_zoom = max(target_zoom, 15)

        radius = map_.planet.attr['orbit']

        self.target_x = -radius * math.cos(map_.planet.pos)
        self.target_y = -radius * math.sin(map_.planet.pos)

        self.set_target_pos(self.target_x, self.target_y)


"""
    MAP CLASS STUFF
"""


class MapWidget(QOpenGLWidget):
    def __init__(self, parent=None):
        super(MapWidget, self).__init__(parent)
        self.zoom = 1.0
        self.camera = Camera(self.width(), self.height())

        # Set the surface format for the widget
        fmt = QSurfaceFormat()
        fmt.setRenderableType(QSurfaceFormat.OpenGL)
        fmt.setProfile(QSurfaceFormat.CoreProfile)
        fmt.setVersion(2, 1)
        self.setFormat(fmt)
        self.makeCurrent()

        # mouse stuff
        self.mouse_x = 0.0
        self.mouse_y = 0.0
        self.prev_mouse_pos = None

        self.setSizePolicy(
            QSizePolicy.Expanding,  # horizontal
            QSizePolicy.Expanding  # vertical
        )

    def initializeGL(self):
        gl.glClearColor(b_r, b_g, b_b, 1.0)
        gl.glMatrixMode(gl.GL_PROJECTION)
        gl.glLoadIdentity()
        gl.glMatrixMode(gl.GL_MODELVIEW)

    def resizeGL(self, w, h):
        # Set the viewport
        gl.glViewport(0, 0, w, h)

        # Set the projection matrix
        gl.glMatrixMode(gl.GL_PROJECTION)
        gl.glLoadIdentity()

        # Set up the projection matrix to match the window size and zoom level
        left = -w / 2.0
        right = w / 2.0
        bottom = -h / 2.0
        top = h / 2.0

        gl.glOrtho(left, right, bottom, top, -1, 1)

        # Set the modelview matrix
        gl.glMatrixMode(gl.GL_MODELVIEW)
        gl.glLoadIdentity()

        self.camera.width = w
        self.camera.height = h

    def wheelEvent(self, event):
        # Calculate the new zoom level
        factor = 1.0 + event.angleDelta().y() / 1200.0
        self.camera.set_zoom(factor)

        # Update the projection matrix
        self.resizeGL(self.width(), self.height())
        # self.camera.apply()

    def mousePressEvent(self, event):
        # Save the current mouse position when the mouse button is pressed
        self.prev_mouse_pos = event.pos()

    def mouseReleaseEvent(self, event):
        # Reset the previous mouse position when the mouse button is released
        self.prev_mouse_pos = None

    def mouseMoveEvent(self, event):
        # If the mouse button is pressed and being moved
        if self.prev_mouse_pos is not None:

            x, y = self.camera.get_pos()

            # Calculate the translation based on the mouse movement
            dx = event.x() - self.prev_mouse_pos.x()
            dy = event.y() - self.prev_mouse_pos.y()

            # Update the translation values
            self.camera.set_pos(x + dx, y - dy)

            # Update the previous mouse position
            self.prev_mouse_pos = event.pos()

            # Update the projection matrix and redraw the scene
            self.resizeGL(self.width(), self.height())
            self.camera.apply()

    def paintInit(self):
        gl.glClear(
            gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT  # type: ignore
        )

        gl.glMatrixMode(gl.GL_MODELVIEW)
        gl.glLoadIdentity()

    def render_text(self, x, x_offset, y, y_offset, text):
        # Set the color to white
        gl.glColor3f(1, 1, 1)

        # Render the text using GLUT
        for i, ch in enumerate(text):
            # Adjust the character positions based on the zoom level
            x_pos = x + (x_offset / self.camera.zoom) + (i * 8 / self.camera.zoom)
            y_pos = y + (y_offset / self.camera.zoom)

            gl.glRasterPos2f(x_pos, y_pos)
            glut.glutBitmapCharacter(
                glut.GLUT_BITMAP_8_BY_13,  # type: ignore
                ord(ch)
            )


class GalaxyMap(MapWidget):
    def __init__(self, galaxy: Union[Galaxy, None]):
        super(GalaxyMap, self).__init__()

        self.galaxy = galaxy
        self.consts = []
        self.stars = []

        glut.glutInit()
        glut.glutInitDisplayMode(
            glut.GLUT_SINGLE | glut.GLUT_RGB  # type: ignore
        )

    def set_galaxy(self, galaxy: Galaxy):
        self.consts = galaxy.get_consts()
        self.stars = galaxy.get_stars()

    def set_const(self, const):
        self.const = const
        if self.const is not None:
            self.camera.zoom_to_fit(self.const, self)

    def set_star(self, star):
        self.star = star
        if self.star is not None:
            self.camera.zoom_to_fit(self.star, self)

    def paintGL(self):
        self.paintInit()

        # update camera position
        self.camera.update_pos()
        self.camera.update_zoom()
        self.camera.apply()

        self.draw_constellations()
        self.draw_stars()

        if self.camera.zoom < 2:
            self.draw_constellation_names()

        if self.camera.zoom >= 2:
            self.draw_star_names()

        # Flush the OpenGL commands to the screen
        gl.glFlush()


    def draw_constellations(self):
        # Set the color to grey
        gl.glColor3f(0.5, 0.5, 0.5)

        for const in self.consts:
            gl.glBegin(gl.GL_LINE_LOOP)
            for point in const.points:
                gl.glVertex(point[0], point[1])
            gl.glEnd()

    def draw_constellation_names(self):
        for const in self.consts:
            x, y = const.pos['x'], const.pos['y']
            x_offset = len(const.name) * 8 / 2
            self.render_text(x, -x_offset, y, 0, const.name)

    def draw_stars(self):
        for star in self.stars:
            # set colour, draw outer star
            x, y = star.pos['x'], star.pos['y']
            colour = star_colours[star.attr['colour']]
            radius = 2
            draw_circle(x, y, radius, colour, zoom=self.camera.zoom)

    def draw_star_names(self):
        for star in self.stars:
            x, y = star.pos['x'], star.pos['y']
            self.render_text(x, 5, y, 5, star.name)


class SystemMap(MapWidget):
    def __init__(self, star: Union[Star, None]):
        super(SystemMap, self).__init__()

        # init objects
        self.star = star
        if self.star is not None:
            self.planets = self.star.orbitals

        # init graphics settings
        self.orbit_scale = 800

    def set_star(self, star):
        self.star = star
        self.planets = self.star.orbitals
        self.planet = None

        if self.star is not None:
            orbits = [
                planet.attr['orbit'] for planet
                in self.star.orbitals  # type: ignore
                if planet.attr['type'] == "Planet"
            ]
            max_orbit = max(orbits)

            for planet in self.star.orbitals:
                planet.attr['orbit'] *= self.orbit_scale / max_orbit

                for moon in planet.orbitals:
                    moon.attr['orbit'] *= self.orbit_scale / max_orbit

            self.camera.zoom_to_fit(self.star, self)

    def set_planet(self, planet):
        self.planet = planet
        if self.planet is not None:
            self.camera.zoom_to_fit(self.planet, self)

    def paintGL(self):
        self.paintInit()

        # update camera position
        self.camera.update_pos()
        self.camera.update_zoom()
        self.camera.apply()

        # draw the star and orbits
        if self.star is not None:
            self.draw_star()
            self.draw_planet_orbits()
            self.draw_planets()
            self.draw_planet_names()

            if self.camera.zoom >= 15:
                self.draw_moon_orbits()
                self.draw_moons()
                self.draw_moon_names()

        # Flush the OpenGL commands to the screen
        gl.glFlush()


    def draw_star(self):
        # Set the color to white
        # set colour, draw outer star
        colour = star_colours[self.star.attr['colour']]  # type: ignore
        radius = 10
        draw_circle(0, 0, radius, colour, zoom=self.camera.zoom)

    def get_orbital_coordinates(self, ox, oy, orbital):
        radius = orbital.attr['orbit']
        x = ox + radius * math.cos(orbital.pos)
        y = oy + radius * math.sin(orbital.pos)

        return x, y

    def draw_planet_orbits(self):
        # Apply zoom and translation transformations
        star_pos = (0, 0)

        # Draw the orbits
        for planet in self.planets:
            if planet.attr['type'] == "Planet":
                colour = [0.5, 0.5, 0.5, 1.0]
                gl.glColor3f(0.5, 0.5, 0.5)
                radius = planet.attr['orbit']
                draw_circle_line(
                    star_pos[0], star_pos[1], radius, colour,
                    segments=1024, zoom=self.camera.zoom
                )

    def draw_planets(self):
        for planet in self.planets:
            colour = [1.0, 1.0, 1.0, 1.0]
            x, y = self.get_orbital_coordinates(0, 0, planet)
            radius = 5
            draw_circle(x, y, radius, colour, zoom=self.camera.zoom)

    def draw_planet_names(self):
        for planet in self.planets:
            x, y = self.get_orbital_coordinates(0, 0, planet)
            offset = 5
            self.render_text(x, offset, y, offset, planet.name)

    def draw_moon_orbits(self):

        for planet in self.planets:
            px, py = self.get_orbital_coordinates(0, 0, planet)

            if planet.attr['type'] == "Planet":

                for moon in planet.get_moons():
                    colour = [0.5, 0.5, 0.5, 1.0]
                    gl.glColor3f(0.5, 0.5, 0.5)
                    radius = moon.attr['orbit']
                    draw_circle_line(
                        px, py, radius, colour,
                        segments=1024, zoom=self.camera.zoom
                    )

    def draw_moons(self):
        for planet in self.planets:
            px, py = self.get_orbital_coordinates(0, 0, planet)
            for moon in planet.get_moons():
                colour = [1.0, 1.0, 1.0, 1.0]
                radius = 3
                x, y = self.get_orbital_coordinates(px, py, moon)
                draw_circle(x, y, radius, colour, zoom=self.camera.zoom)

    def draw_moon_names(self):
        for planet in self.planets:
            px, py = self.get_orbital_coordinates(0, 0, planet)
            for moon in planet.get_moons():
                x, y = self.get_orbital_coordinates(px, py, moon)
                offset = 5
                self.render_text(x, offset, y, offset, moon.name)


"""
    PRIMITIVE FUNCTIONS
"""


def draw_circle(
        cx: float, cy: float, radius: float, colour: list,
        segments: int = 20, zoom: float = 1.0):
    radius /= zoom
    # Create a list of vertices for the circle
    vertices = [(cx, cy)]
    for i in range(segments + 1):
        angle = i * 2.0 * math.pi / segments
        x = cx + radius * math.cos(angle)
        y = cy + radius * math.sin(angle)
        vertices.append((x, y))

    # Convert the vertices list to a numpy array for use with VBOs
    vertices_array = np.array(vertices, dtype=np.float32)

    # Create the VBOs
    vbo_id = gl.glGenBuffers(1)
    gl.glBindBuffer(gl.GL_ARRAY_BUFFER, vbo_id)
    gl.glBufferData(
        gl.GL_ARRAY_BUFFER,
        vertices_array.nbytes,
        vertices_array,
        gl.GL_STATIC_DRAW
    )

    # Enable blending
    gl.glEnable(gl.GL_BLEND)
    gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)

    # Bind the VBO and set up the vertex attributes
    gl.glBindBuffer(gl.GL_ARRAY_BUFFER, vbo_id)
    gl.glEnableClientState(gl.GL_VERTEX_ARRAY)
    gl.glVertexPointer(2, gl.GL_FLOAT, 0, None)

    # Set the color of the circle
    gl.glColor4f(colour[0], colour[1], colour[2], colour[3])

    # Draw the circle with a radial gradient
    gl.glDrawArrays(gl.GL_TRIANGLE_FAN, 0, segments + 2)

    # Disable the vertex attributes and unbind the VBO
    gl.glDisableClientState(gl.GL_VERTEX_ARRAY)
    gl.glBindBuffer(gl.GL_ARRAY_BUFFER, 0)

    # Delete the VBO
    gl.glDeleteBuffers(1, [vbo_id])

    # Disable blending when not needed
    gl.glDisable(gl.GL_BLEND)

def draw_circle_line(
        cx: float, cy: float, radius: float, colour: list,
        segments: int = 20, zoom: float = 1.0):
    radius /= zoom
    # Create a list of vertices for the circle
    vertices = []
    for i in range(segments):
        angle = i * 2.0 * math.pi / segments
        x = cx + radius * math.cos(angle) * zoom
        y = cy + radius * math.sin(angle) * zoom
        vertices.append((x, y))
    # Add the first vertex again to close the loop
    vertices.append(vertices[0])

    # Convert the vertices list to a numpy array for use with VBOs
    vertices_array = np.array(vertices, dtype=np.float32)

    # Create the VBOs
    vbo_id = gl.glGenBuffers(1)
    gl.glBindBuffer(gl.GL_ARRAY_BUFFER, vbo_id)
    gl.glBufferData(
        gl.GL_ARRAY_BUFFER,
        vertices_array.nbytes,
        vertices_array,
        gl.GL_STATIC_DRAW
    )

    # Enable blending
    gl.glEnable(gl.GL_BLEND)
    gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)

    # Bind the VBO and set up the vertex attributes
    gl.glBindBuffer(gl.GL_ARRAY_BUFFER, vbo_id)
    gl.glEnableClientState(gl.GL_VERTEX_ARRAY)
    gl.glVertexPointer(2, gl.GL_FLOAT, 0, None)

    # Set the color of the circle
    gl.glColor4f(colour[0], colour[1], colour[2], colour[3])

    # Draw the circle as a line loop
    gl.glDrawArrays(gl.GL_LINE_LOOP, 0, segments + 1)

    # Disable the vertex attributes and unbind the VBO
    gl.glDisableClientState(gl.GL_VERTEX_ARRAY)
    gl.glBindBuffer(gl.GL_ARRAY_BUFFER, 0)

    # Delete the VBO
    gl.glDeleteBuffers(1, [vbo_id])

    # Disable blending when not needed
    gl.glDisable(gl.GL_BLEND)
