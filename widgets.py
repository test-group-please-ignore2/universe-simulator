# ui libraries
from PyQt5.QtWidgets import QLabel, QListWidget, QSizePolicy, QTableWidget, QVBoxLayout, QWidget
from PyQt5.QtCore import pyqtSignal

from misc import add_list_to_QTable


class ConstList(QWidget):
    const_selected_signal = pyqtSignal(object)

    def __init__(self):
        super().__init__()

        contentLayout = QVBoxLayout()
        contentLayout.addWidget(QLabel('Constellations:'))
        self.consts = QListWidget()
        contentLayout.addWidget(self.consts)
        self.setLayout(contentLayout)

        self.setSizePolicy(
            QSizePolicy.Fixed,  # horizontal
            QSizePolicy.Expanding  # vertical
        )

        self.consts.itemClicked.connect(self.item_clicked)

    def set_galaxy(self, galaxy):
        self.galaxy = galaxy
        self.consts.clear()
        for const in self.galaxy.get_consts():
            self.consts.addItem(const.name)

    def item_clicked(self, item):
        const = self.galaxy.get_const(item.text())
        self.const_selected_signal.emit(const)


class StarList(QWidget):
    star_selected_signal = pyqtSignal(object)

    def __init__(self):
        super().__init__()

        contentLayout = QVBoxLayout()
        contentLayout.addWidget(QLabel('Stars:'))
        self.stars = QListWidget()
        contentLayout.addWidget(self.stars)
        self.setLayout(contentLayout)

        self.setSizePolicy(
            QSizePolicy.Fixed,  # horizontal
            QSizePolicy.Expanding  # vertical
        )

        self.stars.itemClicked.connect(self.item_clicked)

    def set_const(self, const):
        self.const = const
        self.stars.clear()
        for star in self.const.stars:
                self.stars.addItem(star.name)

    def item_clicked(self, item):
        star = self.const.get_star(item.text())
        self.star_selected_signal.emit(star)

class StarInfo(QWidget):

    def __init__(self):
        super().__init__()

        contentLayout = QVBoxLayout()
        contentLayout.addWidget(QLabel('Star Info:'))
        self.info = QTableWidget()
        self.info.horizontalHeader().hide()
        self.info.verticalHeader().hide()
        self.info.setShowGrid(False)
        contentLayout.addWidget(self.info)
        self.setLayout(contentLayout)

        self.setSizePolicy(
            QSizePolicy.Fixed,  # horizontal
            QSizePolicy.Expanding  # vertical
        )

    def set_galaxy(self, game):
        self.galaxy = game.galaxy

    def update_info(self, star):
        # empty bucket for star info lines
        starInfoTable = []

        # add star name
        starInfoTable.append(
            ["Name:", star.name]
        )

        # iterate over read attributes and insert into bucket
        for key, value in star.read_attr.items():
            line = [f"{key.title()}:", value]
            starInfoTable.append(line)

        # put star info list into table
        add_list_to_QTable(self.info, starInfoTable)


class PlanetList(QWidget):
    planet_selected_signal = pyqtSignal(object)

    def __init__(self):
        super().__init__()

        contentLayout = QVBoxLayout()
        contentLayout.addWidget(QLabel('Planet:'))
        self.planets = QListWidget()
        contentLayout.addWidget(self.planets)
        self.setLayout(contentLayout)

        self.setSizePolicy(
            QSizePolicy.Fixed,  # horizontal
            QSizePolicy.Expanding  # vertical
        )

        self.planets.itemClicked.connect(self.item_clicked)

    def set_star(self, star):
        self.planets.clear()
        self.star = star
        for planet in self.star.orbitals:
            self.planets.addItem(planet.name)

        self.update()

    def item_clicked(self, item):
        for planet in self.star.orbitals:
            if planet.name == item.text():
                self.planet_selected_signal.emit(planet)
                return


class PlanetInfo(QWidget):

    def __init__(self):
        super().__init__()

        contentLayout = QVBoxLayout()
        contentLayout.addWidget(QLabel('Planet Info:'))
        self.info = QTableWidget()
        self.info.horizontalHeader().hide()
        self.info.verticalHeader().hide()
        self.info.setShowGrid(False)
        contentLayout.addWidget(self.info)
        self.setLayout(contentLayout)

        self.setSizePolicy(
            QSizePolicy.Fixed,  # horizontal
            QSizePolicy.Expanding  # vertical
        )

    def update_info(self, planet):
        # empty bucket for planet info lines
        planetInfoTable = []

        # add star name
        planetInfoTable.append(
            ["Name:", planet.name]
        )

        # iterate over read attributes and insert into bucket
        for key, value in planet.read_attr.items():
            line = [f"{key.title()}:", value]
            planetInfoTable.append(line)

        # put star info list into table
        add_list_to_QTable(self.info, planetInfoTable)


class LocationInfo(QWidget):

    def __init__(self):
        super().__init__()

        contentLayout = QVBoxLayout()
        contentLayout.addWidget(QLabel('Current Location:'))

        self.info = QTableWidget()
        self.info.horizontalHeader().hide()
        self.info.verticalHeader().hide()
        self.info.setShowGrid(False)
        contentLayout.addWidget(self.info)

        self.setLayout(contentLayout)

        self.setSizePolicy(
            QSizePolicy.Fixed,  # horizontal
            QSizePolicy.Expanding  # vertical
        )

    def update_info(self, star, planet):

        star = star.name if star is not None else ""
        planet = planet.name if planet is not None else ""

        add_list_to_QTable(
            self.info,
            [
                ["System:", star],
                ["Planet:", planet]
            ]
        )