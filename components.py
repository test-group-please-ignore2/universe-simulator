from dataclasses import dataclass, field
from random import random


@dataclass
class Weapon:
    name: str
    damage: int
    shots: int
    accuracy: int

    def attack(self, skill, target):
        messages = []
        hits = 0
        total = 0

        for _ in range(self.shots):
            hit_chance = skill * self.accuracy / 100 * 0.99
            dice = random() * 100
            if dice < hit_chance:
                hits += 1
                total += self.damage
                target.take_damage(self.damage)

        if hits > 0:
            messages.append(f" > {hits} shots hit for {total} damage!")

        else:
            messages.append(f" > All shots miss...")

        return messages


@dataclass
class Inventory:
    max_items: int
    items: list = field(default_factory=list)

    def add_item(self, item):
        if len(self.items) >= self.max_items:
            return False

        self.items.append(item)
        return True

    def remove_item(self, item):
        if item in self.items:
            self.items.remove(item)
            return True

        return False
