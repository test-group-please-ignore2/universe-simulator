# python libraries
import pandas as pd
import random

from pandas.core.frame import DataFrame
from dataclasses import dataclass, field
from typing import Union

from pandas.io.formats.format import math

# my libraries
import maths
import lists

from misc import ave, sci_notation

# set list variables
terrestrial_types = pd.read_csv(lists.terrestrial_types)
atmosphere_types = pd.read_csv(lists.atmosphere_types)


@dataclass
class Galaxy:
    """
    This class defines a galaxy, or a group of constellations.

    Parameters:
    -----------
        consts : list
            A list of stars in the Constellation.

    """
    consts: list = field(default_factory=list)

    def get_consts(self):
        return self.consts

    def get_const(self, name: str):
        return next((c for c in self.consts if c.name == name), None)

    def get_stars(self):
        return [star for const in self.consts for star in const.stars]

    def get_star(self, name: str):
        return next((s for s in self.get_stars() if s.name == name), None)

@dataclass
class Constellation:
    """
    This class defines a constellation, or a group of stars.

    Parameters:
    -----------
        name : str
            Something cool. Randomly Generated (TM).

        pos : dict
            x, y of the Constellation's centroid within the galaxy.

        points : list
            A list of x, y coordinates of the vertices of the Constellation.
            I think.

        stars : list
            A list of stars in the Constellation.

    """
    name:   str = ""
    pos:    dict = field(default_factory=dict)
    points: list = field(default_factory=list)
    stars:  list = field(default_factory=list)

    def get_stars(self):
        return self.stars

    def get_star(self, name: str):
        return next((s for s in self.stars if s.name == name), None)


@dataclass
class Celestial:
    """
    This class defines the basic 'Celestial' object, which is any orbiting
    stellar body, i.e. a star, planet or moon.

    Parameters:
    -----------
        name : str
            The name of the Celestial object.

        orbitals : list
            A list of Celestial objects orbiting the Celestial.

        attr : dict
            A dict of the Celestial's attributes.

        read_attr : dict
            A dict of the Celestial's attributes, formatted for printing.

    """
    name:       str = ""
    orbitals:   list = field(default_factory=list)
    attr:       dict = field(default_factory=dict)
    read_attr:  dict = field(default_factory=dict)

    def planet_orbital(self,
                       n: int,
                       last_orbit: Union[float, None]) -> 'Planet':
        """
        Creates a new Planet orbiting the Celestial.

        Parameters:
        -----------
            n : int
                The number of the Planet within the solar system.

            last_orbit : float
                The distance of the last planet from the Celestial.

        Returns:
        --------
            new_orbital : Planet
                The shiny new planet.

        """

        # set default values for orbit distance and new orbital
        min_orbit_dist, max_orbit_dist = 0, 0
        min_orbit_dist = 0.05
        max_orbit_dist = 5

        # calculate orbit values
        distance = round(
            random.uniform(
                min_orbit_dist,
                max_orbit_dist
            ), 5  # round to 5 decimal places
        )

        # if there was a previous planet, put the new one further away
        if last_orbit:
            distance = round(distance + last_orbit, 5)

        # we might do eccentricity one day but for now let's not
        eccentricity = 0  # round(random.uniform(0, 0.75), 2)
        pos = math.radians(random.randint(1, 360))
        # create the empty Planet object
        new_orbital = Planet(pos=pos)

        # update attributes
        new_orbital.attr.update(
            {
                "type":
                "Planet",

                "class":
                None,

                "orbit":
                distance,

                "eccentricity":
                eccentricity
            }
        )

        # update read attributes
        new_orbital.read_attr.update(
            {
                "type":
                f"{new_orbital.attr['type']}",

                "class":
                f"{new_orbital.attr['class']}",

                "orbit":
                f"{new_orbital.attr['orbit']:,} AU",

                "eccentricity":
                f"{new_orbital.attr['eccentricity']}"
            }
        )

        # name planets with roman numerals
        new_orbital.name = f"{self.name} {ave(n + 1)}"

        return new_orbital

    def moon_orbital(self,
                     n: int,
                     last_orbit: float = 0) -> 'Moon':
        """
        This function creates a new moon orbiting the Celestial.

        Parameters:
        -----------
            n : int
                The number of the moon within the planetary system.

            last_orbit : float
                The distance of the last planet from the Celestial.

        """

        # set default values for orbit distance and new orbital
        min_orbit_dist, max_orbit_dist = 0, 0
        min_orbit_dist = 0.0005
        max_orbit_dist = 0.05

        # calculate orbital distances
        distance = round(random.uniform(
            min_orbit_dist, max_orbit_dist), 5)

        # override if there's a previous orbit
        if not last_orbit:
            distance = round(distance + last_orbit, 5)

        # let's not worry about this either
        eccentricity = 0  # round(random.uniform(0, 0.75), 2)
        pos = math.radians(random.randint(1, 360))
        # create the new Moon object
        new_orbital = Moon(pos=pos)

        # update attributes
        new_orbital.attr.update(
            {
                "type":
                "Moon",

                "class":
                None,

                "orbit":
                distance,

                "eccentricity":
                eccentricity
            }
        )

        # update read attributes
        new_orbital.read_attr.update(
            {
                "type":
                f"{new_orbital.attr['type']}",

                "class":
                f"{new_orbital.attr['class']}",

                "orbit":
                f"{new_orbital.attr['orbit']:,} AU",

                "eccentricity":
                f"{new_orbital.attr['eccentricity']}"
            }
        )

        # also Roman numerals for moons
        new_orbital.name = f"{self.name} Moon {ave(n + 1)}"

        return new_orbital


@dataclass
class Star(Celestial):
    """
    This class gives the Celestial object star-like qualities, such as
    colour, temperature, luminosity, inner and outer habitable zone ranges.

    Parameters:
    -----------
        pos : dict
            I think this is unused, but I'm passing it so let's leave it.

    """
    pos:    dict = field(default_factory=dict)

    def get_planets(self):
        return self.orbitals

    def get_planet(self, name):
        return next((p for p in self.orbitals if p.name == name), None)

    def generate_star(self, class_: str, type_list: DataFrame) -> None:
        """
        This function provides the Star object with it's star-like qualities.

        Parameters:
            class_ : str
                The class of the Star.

            type_list : DataFrame
                The datasheet for stars.

        """

        # calculate stellar attributes
        index = type_list.index[type_list['Class'] == class_].tolist()[0]
        minMass = type_list.at[index, 'minMass']
        maxMass = type_list.at[index, 'maxMass']
        mass = round(random.uniform(minMass, maxMass), 5)
        radius = maths.stellar_radius(mass)
        luminosity = maths.stellar_luminosity(mass)
        temperature = maths.stellar_temperature(mass)
        inner_HZ, outer_HZ = maths.stellar_habitable_zone(luminosity)
        colour = type_list.at[index, "Colour"]
        has_habitable = False

        """
        I'm making these into attributes and read-attributes so that if
        I have to print them anywhere, I can just do the formatting here
        """

        # update attributes
        self.attr.update(
            {
                "type":
                "Star",

                "class":
                class_,

                "mass":
                mass,

                "radius":
                radius,

                "colour":
                colour,

                "luminosity":
                luminosity,

                "temperature":
                temperature,

                "inner_HZ":
                inner_HZ,

                "outer_HZ":
                outer_HZ,

                "has_habitable":
                has_habitable
            }
        )

        # update read attributes
        self.read_attr.update(
            {
                "type":
                f"{self.attr['type']}",

                "class":
                f"{self.attr['class']}",

                "mass":
                f"{self.attr['mass']:,} M☉",

                "radius":
                f"{self.attr['radius']:,} R☉",

                "colour":
                f"{self.attr['colour']}",

                "luminosity":
                f"{self.attr['luminosity']}",

                "temperature":
                f"{self.attr['temperature']:,} °K",
                "habitable zone":
                f"{self.attr['inner_HZ']:.6f} - " +
                f"{self.attr['outer_HZ']:.6f} AU",

                "has habitable":
                f"{self.attr['has_habitable']}"
            }
        )


@dataclass
class Planet(Celestial):
    """
    This class gives the Celestial object planet-like qualities, such as
    composition, and more things that I will do later.

    Parameters:
    -----------
        pos : int
            The angle of the Planet around it's Star

    """
    pos: float = 0

    def get_moons(self):
        return self.orbitals

    def get_moon(self, name: str):
        return next((m for m in self.orbitals if m.name == name), None)

    def generate_planet(self, inner_HZ: float, outer_HZ: float) -> None:
        """
        This function provides the Planet object with it's composition specific
        planet-like qualities.

        Parameters:
        -----------
            inner_HZ : float
                The inner bound of the parent Star's habitable zone.

            outer_HZ : float
                The outer bound of the parent Star's habitable zone.

        """

        # update attributes
        self.attr.update(
            {
                "composition":
                None
            }
        )

        # update read attributes
        self.attr.update(
            {
                "composition":
                f"{self.attr['composition']}"
            }
        )

        # randomly select whether the planet will be gaseous or terrestrial
        comp_check = random.randint(1, 4)

        if 1 <= comp_check <= 3:
            composition = "Gaseous"

        else:
            composition = "Terrestrial"

        # assign planetary attributes
        radius = maths.planetary_radius(composition)
        density = maths.planetary_density(composition)
        mass = maths.planetary_mass(radius, density)

        # update attributes
        self.attr.update(
            {
                "class":
                None,

                "composition":
                composition,

                "radius":
                radius,

                "density":
                density,

                "mass":
                mass
            }
        )

        # update read attributes
        self.read_attr.update(
            {
                "class":
                f"{self.attr['class']}",

                "composition":
                f"{self.attr['composition']}",

                "radius":
                f"{self.attr['radius']:,} km",

                "density":
                f"{self.attr['density']:,} kg/m³",

                "mass":
                f"{sci_notation(self.attr['mass'])} kg"
            }
        )

        # generate planetary attributes based on composition
        if self.attr['composition'] == "Gaseous":
            self.generate_gas_planet()

        elif self.attr['composition'] == "Terrestrial":
            self.generate_terrestrial_planet(inner_HZ, outer_HZ)

    def generate_gas_planet(self) -> None:
        """
        This function adds gas attributes to a Planet object.

        Parameters:
        -----------
            None

        Returns:
        --------
            None

        """

        """
        TODO: Make the below a bit more in depth, look at calculating
        core density and mass vs. atmosphere density and maths, or gas
        layers or something. Orbital distance weighting would be nice too.
        """

        if 600 <= self.attr['density'] < 900:
            gas = "Hydrogen"

        elif 900 <= self.attr['density'] < 1500:
            gas = "Helium"

        elif 1500 <= self.attr['density'] < 2100:
            gas = "Liquid Hydrogen"

        elif 2100 <= self.attr['density'] < 3000:
            gas = "Liquid Helium"

        else:
            gas = "Exotic"

        if 10000 <= self.attr['radius'] <= 25000:
            size = "Dwarf"

        else:
            size = "Giant"

        habitability = 0

        self.attr.update(
            {
                "class":
                f"{gas} {size}",

                "habitability":
                habitability
            }
        )

        self.read_attr.update(
            {
                "class":
                f"{self.attr['class']}",

                "habitability":
                f"{self.attr['habitability']}"
            }
        )

        # self.type_ = f"{gas} {size}"

    def generate_terrestrial_planet(self,
                                    inner_HZ: float,
                                    outer_HZ: float) -> None:
        """
        This function adds terrestrial attributes to a Planet object.

        Parameters:
        -----------
            inner_HZ : float
                The inner bound of the parent Star's habitable zone.

            outer_HZ : float
                The outer bound of the parent Star's habitable zone.

        Returns:
        --------
            None

        """

        # get planet types for probability weighting
        types = terrestrial_types['Class'].tolist()  # type: ignore

        # set default value for type_prob
        type_prob = None

        if self.attr['orbit'] < inner_HZ:
            type_prob = terrestrial_types['Inner_Prob'] \
                .tolist()

        elif inner_HZ <= self.attr['orbit'] <= outer_HZ:
            type_prob = terrestrial_types['HZ_Prob'] \
                .tolist()

        elif outer_HZ < self.attr['orbit']:
            type_prob = terrestrial_types['Outer_Prob'] \
                .tolist()

        # class stuff
        class_ = random.choices(types, weights=type_prob, k=1)[0]

        i = terrestrial_types.index[terrestrial_types['Class'] == class_] \
            .tolist()[0]

        # atmosphere stuff
        atm_chance = terrestrial_types.at[i, 'Atm_Chance']

        if random.randint(0, 99) < atm_chance:
            primary_list = terrestrial_types.at[i, 'Atm_Primary'] \
                .split(",")

            secondary_list = terrestrial_types.at[i, 'Atm_Secondary'] \
                .split(",")

            trace_list = terrestrial_types.at[i, 'Atm_Trace'] \
                .split(",")

            atm_primary = random.choice(primary_list)

            # make sure that sure that the primary and secondary are different
            atm_secondary = atm_primary
            while atm_secondary == atm_primary:
                atm_secondary = random.choice(secondary_list)

            # make sure that the trace is different too
            atm_trace = atm_primary
            while atm_trace == atm_primary or atm_trace == atm_secondary:
                atm_trace = random.sample(
                                trace_list,
                                random.randint(1, len(trace_list)))

                atm_trace = ', '.join(str(x) for x in atm_trace)

        # else no atmosphere
        else:
            atm_primary, atm_secondary, atm_trace = "None", "None", "None"

        # need to do a major rework of habitability
        habitability = terrestrial_types.loc[
            terrestrial_types['Class'] == class_,
            'Habitability'] \
            .iloc[0]

        # update attributes
        self.attr.update(
            {
                "class":
                class_,

                "atm. primary":
                atm_primary,

                "atm. secondary":
                atm_secondary,

                "atm. trace":
                atm_trace,

                "habitability":
                habitability
            }
        )

        # update read attributes
        self.read_attr.update(
            {
                "class":
                f"{self.attr['class']}",

                "atm. primary":
                f"{self.attr['atm. primary']}",

                "atm. secondary":
                f"{self.attr['atm. secondary']}",

                "atm. trace":
                f"{self.attr['atm. trace']}",

                "habitability":
                f"{self.attr['habitability']}"
            }
        )


@dataclass
class Moon(Celestial):
    """
    This class gives the Celestial object moon-like qualities.

    Parameters:
    -----------
        None

    """
    pos: float = 0.0

    def generate_moon(self) -> None:
        """
        This function is largely a placeholder until I fix.

        Parameters:
        -----------
            None

        Returns:
        --------
            None

        """

        # placeholder moon type stuff
        type_roll = random.randint(1, 3)

        if type_roll == 1:
            class_ = "Rocky Moon"

        elif type_roll == 2:
            class_ = "Metallic Moon"

        else:
            class_ = "Icy Moon"

        self.attr.update(
            {
                "class": class_
            }
        )

        self.read_attr.update(
            {
                "class": f"{self.attr['class']}"
            }
        )
