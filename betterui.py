from PyQt5.QtCore import QObject, pyqtSignal
from PyQt5.QtWidgets import QAction, QMainWindow, QMdiArea, QMenuBar
from celestials import Galaxy
from encounters import new_encounter
from procgen import GenerateGalaxy
from engine import GameState, GameWidgets, GameWindows

from windows import OkPopUp, NewGameWindow, ShipBattleWindow, ShipTravelWindow


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        """
            MainWindow Setup Stuff
        """

        # set up main window properties and components
        self.setWindowTitle("Space Pilgrim")
        self.setGeometry(100, 100, 800, 600)

        """
            Menu Setup Stuff
        """

        # create menu bar
        self.menuBar = QMenuBar()
        self.setMenuBar(self.menuBar)

        # game menu
        self.menuGame = self.menuBar.addMenu('Game')
        self.openNewGame = QAction('New Game', self)
        self.menuGame.addAction(self.openNewGame)

        # ship menu
        self.menuShip = self.menuBar.addMenu('Ship')
        self.openShipTravel = QAction('Travel', self)
        self.menuShip.addAction(self.openShipTravel)
        self.openShipBattle = QAction('Battle', self)
        self.menuShip.addAction(self.openShipBattle)

        """
            SubWindow Setup Stuff
        """

        self.mainArea = QMdiArea()
        self.setCentralWidget(self.mainArea)

        """
            Modular Widget Setup
        """

        self.widgets = GameWidgets()
        self.windows = GameWindows(self.mainArea, self.widgets)

        """
            Initialize Subwindows
        """

        self.connect_signals()
        self.windows.newGame.open()

    def connect_signals(self):

        """
            Menu Connections
        """

        # show new game window
        self.openNewGame.triggered.connect(self.windows.newGame.open)

        # show ship travel window
        self.openShipTravel.triggered.connect(self.windows.shipTravel.open)

        # show ship battle window
        self.openShipBattle.triggered.connect(self.windows.shipBattle.open)

        """
            Game Connections
        """

        # generate galaxy
        self.windows.newGame.generateGalaxy.clicked.connect(self.generate_galaxy)


    def generate_galaxy(self):
        """
        This function initializes the user input variables and initiates the
        galaxy generation loop.

        Parameters:
        -----------
            None

        Returns:
        --------
            None

        """

        # clear text console
        self.windows.newGame.textConsole.clear()

        # set max values from inputs
        max_consts = self.windows.newGame.maxConst.value()
        max_stars = self.windows.newGame.maxStars.value()
        max_planets = self.windows.newGame.maxPlanets.value()
        max_moons = self.windows.newGame.maxMoons.value()
        map_size = self.windows.newGame.mapSize.value()

        # run thread
        self.GenerateGalaxyThread = GenerateGalaxy(
            max_consts, max_stars, max_planets, max_moons, map_size)
        self.GenerateGalaxyThread.start()
        self.GenerateGalaxyThread.msg_sig.connect(self.windows.newGame.write)
        self.GenerateGalaxyThread.output_sig.connect(self.set_galaxy)

    def set_galaxy(self, galaxy: Galaxy) -> None:
        """
        This function sets the current galaxy and populates the starList. Could
        be useful when saving/loading galaxys.

        Parameters:
        -----------
            galaxy : list
                A list of Constellations that makes up the galaxy.

        Returns:
        --------
            None

        """

        self.game = GameState(galaxy, self.widgets, self.windows)
        