import sys
import qdarkstyle
from PyQt5.QtWidgets import QApplication
from betterui import MainWindow

# Create the application and show the main window
app = QApplication([])
mainWindow = MainWindow()
mainWindow.show()

app.setStyleSheet(qdarkstyle.load_stylesheet())

# Run the application
app.exec_()
