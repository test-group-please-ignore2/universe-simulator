import math
import numpy as np
import random

from typing import Tuple


def stellar_radius(mass) -> float:
    """
    This function randomly generates the radius of a star based off it's mass.
    """

    radius = mass ** 0.8
    radius = round(random.uniform(radius * 0.9, radius * 1.1), 5)

    return radius


def stellar_luminosity(mass) -> float:
    """
    This function randomly generates the luminosity of a star based off
    it's mass.
    """

    luminosity = mass ** 3.5
    luminosity = round(random.uniform(luminosity * 0.9, luminosity * 1.1), 5)

    return luminosity


def stellar_habitable_zone(luminosity) -> Tuple[float, float]:
    """
    This function randomly generates a habitable zone based on the luminosity
    of the star. Note this function returns two numbers for inner and outer
    radius.
    """

    inner, outer = round((luminosity / 1.3) ** 0.5,
                         5), round((luminosity / 0.4) ** 0.5, 5)

    return inner, outer


def stellar_temperature(mass) -> float:
    """
    This function randomly generates a star's temperature based (roughly)
    off it's mass.
    """
    temperature = 5740 * mass ** 0.54

    temperature = round(random.uniform(
        temperature * 0.9, temperature * 1.1), 0)

    return temperature


def planetary_radius(comp) -> int:
    """
    This function randomly generates a planetary radius depending on the
    planet's composition.
    """

    # set default value for radius
    radius = 0

    if comp == "Terrestrial":
        radius = random.randint(4000, 25000)

    elif comp == "Gaseous":
        radius = random.randint(11000, 100000)

    return radius


def planetary_density(comp) -> int:
    """
    This function randomly generates a planetary density depending on the
    planet's composition.
    """

    # set default value for density
    density = 0

    if comp == "Terrestrial":
        density = random.randint(3900, 5500)

    elif comp == "Gaseous":
        density = random.randint(600, 3000)

    return density


def planetary_mass(radius, density) -> float:
    """
    This function calculates the mass of a planet based on it's radius
    and density.
    """

    mass = (4 / 3) * math.pi * density * (radius ** 3)

    return mass


class PointInPolygon:
    def __init__(self, vertices, dist):
        self.dist = dist
        self.vertices = vertices
        self.n = len(self.vertices)
        self.centroid = self._calculate_centroid()
        self.vertex_distances = self._calculate_vertex_distances()
        self.edge_distances = self._calculate_edge_distances()

    def _calculate_centroid(self):
        x_sum = sum(vertex[0] for vertex in self.vertices)
        y_sum = sum(vertex[1] for vertex in self.vertices)
        x_avg = x_sum / self.n
        y_avg = y_sum / self.n
        return (x_avg, y_avg)

    def _calculate_vertex_distances(self):
        distances = []
        for vertex in self.vertices:
            distance = math.sqrt((vertex[0] - self.centroid[0]) ** 2 +
                                 (vertex[1] - self.centroid[1]) ** 2)
            distances.append(distance)
        return distances

    def _calculate_edge_distances(self):
        distances = []
        for i in range(self.n):
            v1 = self.vertices[i]
            v2 = self.vertices[(i+1) % self.n]
            dx = v2[0] - v1[0]
            dy = v2[1] - v1[1]
            edge_length = math.sqrt(dx ** 2 + dy ** 2)
            edge_norm = (-dy, dx)
            d = (self.centroid[0] - v1[0]) * edge_norm[0] + \
                (self.centroid[1] - v1[1]) * edge_norm[1]
            distance = abs(d) / edge_length
            distances.append(distance)
        return distances

    def _generate_random_point(self, r):
        angle = 2 * math.pi * random.random()
        x = math.cos(angle) * r
        y = math.sin(angle) * r
        return (x + self.centroid[0], y + self.centroid[1])

    def is_point_in_polygon(self, point):
        x, y = point
        inside = False
        for i in range(self.n):
            v1 = self.vertices[i]
            v2 = self.vertices[(i+1) % self.n]
            if ((v1[1] > y) != (v2[1] > y) and
                    x < (v2[0] - v1[0]) * (y - v1[1]) / (v2[1] - v1[1]) + v1[0]):
                inside = not inside
        return inside

    def get_point_in_polygon(self):
        max_radius = min(self.edge_distances) - self.dist
        while True:
            r = max_radius * math.sqrt(random.random())
            point = self._generate_random_point(r)
            if self.is_point_in_polygon(point):
                return point


# For mathematical details of this algorithm, please see the blog
# article at https://scipython.com/blog/poisson-disc-sampling-in-python/
# Christian Hill, March 2017.


class PoissonDisc():
    """A class for generating two-dimensional Possion (blue) noise)."""

    def __init__(self, width=50, height=50, r=1, k=30):
        self.width, self.height = width, height
        self.r = r
        self.k = k

        # Cell side length
        self.a = r/np.sqrt(2)
        # Number of cells in the x- and y-directions of the grid
        self.nx, self.ny = int(width / self.a) + 1, int(height / self.a) + 1

        self.reset()

    def reset(self):
        """Reset the cells dictionary."""

        # A list of coordinates in the grid of cells
        coords_list = [(ix, iy) for ix in range(self.nx)
                       for iy in range(self.ny)]
        # Initilalize the dictionary of cells: each key is a cell's coordinates
        # the corresponding value is the index of that cell's point's
        # coordinates in the samples list (or None if the cell is empty).
        self.cells = {coords: None for coords in coords_list}

    def get_cell_coords(self, pt):
        """Get the coordinates of the cell that pt = (x,y) falls in."""

        return int(pt[0] // self.a), int(pt[1] // self.a)

    def get_neighbours(self, coords):
        """Return the indexes of points in cells neighbouring cell at coords.
        For the cell at coords = (x,y), return the indexes of points in the
        cells with neighbouring coordinates illustrated below: ie those cells
        that could contain points closer than r.
                                     ooo
                                    ooooo
                                    ooXoo
                                    ooooo
                                     ooo
        """

        dxdy = [(-1, -2), (0, -2), (1, -2),
                (-2, -1), (-1, -1), (0, -1),
                (1, -1), (2, -1), (-2, 0),
                (-1, 0), (1, 0), (2, 0),
                (-2, 1), (-1, 1), (0, 1),
                (1, 1), (2, 1), (-1, 2),
                (0, 2), (1, 2), (0, 0)]
        neighbours = []
        for dx, dy in dxdy:
            neighbour_coords = coords[0] + dx, coords[1] + dy
            if not (0 <= neighbour_coords[0] < self.nx and
                    0 <= neighbour_coords[1] < self.ny):
                # We're off the grid: no neighbours here.
                continue
            neighbour_cell = self.cells[neighbour_coords]
            if neighbour_cell is not None:
                # This cell is occupied: store the index of the contained point
                neighbours.append(neighbour_cell)
        return neighbours

    def point_valid(self, pt):
        """Is pt a valid point to emit as a sample?
        It must be no closer than r from any other point: check the cells in
        its immediate neighbourhood.
        """

        cell_coords = self.get_cell_coords(pt)
        for idx in self.get_neighbours(cell_coords):
            nearby_pt = self.samples[idx]
            # Squared distance between candidate point, pt, and this nearby_pt.
            distance2 = (nearby_pt[0]-pt[0])**2 + (nearby_pt[1]-pt[1])**2
            if distance2 < self.r**2:
                # The points are too close, so pt is not a candidate.
                return False
        # All points tested: if we're here, pt is valid
        return True

    def get_point(self, refpt):
        """Try to find a candidate point near refpt to emit in the sample.
        We draw up to k points from the annulus of inner radius r, outer radius
        2r around the reference point, refpt. If none of them are suitable
        (because they're too close to existing points in the sample), return
        False. Otherwise, return the pt.
        """

        i = 0
        while i < self.k:
            i += 1
            rho = np.sqrt(np.random.uniform(self.r**2, 4 * self.r**2))
            theta = np.random.uniform(0, 2*np.pi)
            pt = refpt[0] + rho*np.cos(theta), refpt[1] + rho*np.sin(theta)
            if not (0 <= pt[0] < self.width and 0 <= pt[1] < self.height):
                # This point falls outside the domain, so try again.
                continue
            if self.point_valid(pt):
                return pt
        # We failed to find a suitable point in the vicinity of refpt.
        return False

    def sample(self):
        """Poisson disc random sampling in 2D.
        Draw random samples on the domain width x height such that no two
        samples are closer than r apart. The parameter k determines the
        maximum number of candidate points to be chosen around each reference
        point before removing it from the "active" list.
        """

        # Pick a random point to start with.
        pt = (np.random.uniform(0, self.width),
              np.random.uniform(0, self.height))
        self.samples = [pt]
        # Our first sample is indexed at 0 in the samples list...
        self.cells[self.get_cell_coords(pt)] = 0  # type: ignore
        # and it is active, in the sense that we're going to look for more
        # points in its neighbourhood.
        active = [0]

        # As long as there are points in the active list, keep looking for
        # samples.
        while active:
            # choose a random "reference" point from the active list.
            idx = np.random.choice(active)
            refpt = self.samples[idx]
            # Try to pick a new point relative to the reference point.
            pt = self.get_point(refpt)
            if pt:
                # Point pt is valid: add it to samples list and mark as active
                self.samples.append(pt)
                nsamples = len(self.samples) - 1
                active.append(nsamples)
                self.cells[self.get_cell_coords(pt)] = nsamples  # type: ignore
            else:
                # We had to give up looking for valid points near refpt, so
                # remove it from the list of "active" points.
                active.remove(idx)

        return self.samples

