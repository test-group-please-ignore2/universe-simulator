+------------------+
| COMBAT MECHANICS |
+------------------+

HP Pool:

	- Armor
		Fairly safe to take damage.
		May be ablative or have other modifiers.

	- Hull
		Can tank damage, not great though.
		May result in decompression if breached, additional damage rolls here.

	- Structure
		Invariably bad if damaged.
		Affects components, structural integrity.

Weapons Phase:

	- Roll to Hit:
		Simulates accuracy of firing solution based on:
			- Gunnery Skill
			- Attacker / Target movement
			- EWAR duel affects hit chance

	- Hit Location:
		Random if not targeted shot

	- Roll to Damage:
		Glancing, Clean, Penetrating Hits
		Penetrating hits do low damage to the layer hit, but high damage to the level beneath
			- Can result in overpenetration if penetrating hit on structure
		High chance to cause breaches

Weapon Types:
	
	Close Range:
		- CIWS
			Good for swarming type threats
			Chemically propelled ballistic weapons
			High ROF, low damage, low velocity
			Really only good for PD roles
			Ammo Types:
				Flak - good for evasive threats
				AP - good for armoured threats

		- Coil Guns
			Good anti fighter weapon
			Med ROF, med damage, med velocity
			Ammo Types:
				Kinetic - anti fighter

	Med Range:
		- Laser PD
			Single target, only effective against missiles/torpedoes
			Instant (ish) effect on target

		- Missiles
			Guided or unguided
			Long delay effect on target
			Ammo Types:
				AP
				Inferno
				LK
				EMP

		- Rail Guns
			Low ROF, med damage, med velocity

	Long Range:
		- Torpedoes
			Long range anti capital weapon
			Warps the torpedo within range of the target, burns to impact
			Very low ROF, High Damage
			Very high energy cost, can only target large grav signatures 


Player Actions:

	Before the Turn begins, both ships select a Stance. The basic three stances are:

	Cautious:
		+ chance to hit
		+ flux
		- evasion

	Standard:
		0 chance to hit
		0 flux
		0 evasion

	Reckless:
		- chance to hit
		+ flux
		+ evasion

	Most Stances that aren't Standard will increase the ships EM or Heat flux,
	which will need to be dissipated or bad things will happen. Additionally,
	there will be specialty Stances that can be earned through captain experience,
	or are specific to a ship. These can have very different effects, for example:

	Feint Attack:
	+ chance to hit
	+++ flux
	+ evasion

	Vent Flux:
	- can't shoot
	-- flux
	-- enemy chance to hit

	Overclock Weapons:
	++ double fire rate
	+++ flux
	-- chance to jam/malfunction

	Some of these special effects can be mitigated.

