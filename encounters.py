# py libraries
from dataclasses import dataclass
import random

# my libraries
from celestials import Star, Planet


@dataclass
class Encounter():
    star: Star
    planet: Planet
    # text = ""


@dataclass
class CombatEncounter(Encounter):
    pass


@dataclass
class TradeEncounter(Encounter):
    def __post_init__(self):
        name = self.planet.name
        self.title = "Rude Trading Convoy"
        self.text = (
            f"You notice a trading convoy orbiting {name}, " 
            f"but they don't respond to your hails..."
        )

@dataclass
class FlavourEncounter(Encounter):
    def __post_init__(self):
        name = self.planet.name
        class_ = self.planet.read_attr['class']
        comp = self.planet.read_attr['composition']
        planet = "" if comp == "Gaseous" else " planet"
        self.title = f"Arrival at {name}"
        self.text = f"{name} is an unremarkable {class_}{planet}."


@dataclass
class SalvageEncounter(Encounter):
    pass


def new_encounter(star: Star, planet: Planet):
    roll = random.randint(1,2)

    if roll == 1:
        encounter = FlavourEncounter(star, planet)

    else:
        encounter = TradeEncounter(star, planet)
        
    return encounter