# misc function and other stuff
from PyQt5.QtWidgets import QTableWidget, QTableWidgetItem
from collections import OrderedDict
import random


def ave(num: int):
    """
    This function converts an integer into a roman numeral.
    """
    roman = OrderedDict()
    roman[1000] = "M"
    roman[900] = "CM"
    roman[500] = "D"
    roman[400] = "CD"
    roman[100] = "C"
    roman[90] = "XC"
    roman[50] = "L"
    roman[40] = "XL"
    roman[10] = "X"
    roman[9] = "IX"
    roman[5] = "V"
    roman[4] = "IV"
    roman[1] = "I"

    def roman_num(num):
        for r in roman.keys():
            x, y = divmod(num, r)  # type: ignore
            yield roman[r] * x
            num -= (r * x)
            if num <= 0:
                break

    return "".join([a for a in roman_num(num)])


def khaire(num):
    """
    This function converts a number into a greek letter.
    """

    greek = {
        1: "Alpha",
        2: "Beta",
        3: "Gamma",
        4: "Delta",
        5: "Epsilon",
        6: "Zeta",
        7: "Eta",
        8: "Theta",
        9: "Iota",
        10: "Kappa",
        11: "Lambda",
        12: "Mu",
        13: "Nu",
        14: "Xi",
        15: "Omicron",
        16: "Pi",
        17: "Rho",
        18: "Sigma",
        19: "Tau",
        20: "Upsilon",
        21: "Phi",
        22: "Chi",
        23: "Psi",
        24: "Omega"
    }

    return greek.get(num)


def csv_to_list(path, file):

    output = []
    with open(path + file, "r", encoding='latin-1') as f:
        for line in f:
            line = line.strip("\n")
            output.append(line)

    return output


def csv_to_dict(path, file):

    output = {}
    with open(path + file, "r") as f:
        for line in f:
            line = line.strip("\n")
            values = line.split(",")
            output[values[0]] = values[1]

    return output


def to_au(num):
    num = round(num / 149597870.7, 5)

    return num


def if_au(num, unit):

    if num >= 1000000:
        num = round(num / 149597870.7, 5)
        unit = "AU"
    else:
        unit = "km"

    if unit:
        return num, unit

    if not unit:
        return num


def weighted_random_by_dict(dict):
    rand_val = random.random()
    total = 0
    for k, v in dict.items():
        total += v
        if rand_val <= total:
            return k

    assert False, 'unreachable'


def SuperScriptinate(number):

    return number.replace('0', '⁰')\
        .replace('1', '¹')\
        .replace('2', '²')\
        .replace('3', '³')\
        .replace('4', '⁴')\
        .replace('5', '⁵')\
        .replace('6', '⁶')\
        .replace('7', '⁷')\
        .replace('8', '⁸')\
        .replace('9', '⁹')\
        .replace('-', '⁻')


def sci_notation(number, sig_fig=2):
    ret_string = "{0:.{1:d}e}".format(number, sig_fig)
    a, b = ret_string.split("e")
    b = int(b)         # removed leading "+" and strips leading zeros too.
    return a + "×10" + SuperScriptinate(str(b))


def add_list_to_QTable(table: QTableWidget, list_add: list):
    """
    This function adds items to a PyQt table widget, expanding it to fit.
    """

    # get length and width of table
    table_x = len(list_add[0])
    table_y = len(list_add)

    # set PyQt table to same size
    table.setColumnCount(table_x)
    table.setRowCount(table_y)

    for y in range(table_y):
        for x in range(table_x):
            table.setItem(y, x,
                          QTableWidgetItem(str(list_add[y][x])))

    table.resizeColumnsToContents()
