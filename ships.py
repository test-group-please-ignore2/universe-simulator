from dataclasses import dataclass
from components import Inventory, Weapon


@dataclass
class Ship:
    name: str
    armour: int
    hull: int
    struct: int
    skill: int
    weapons: list[Weapon]
    # inventory: Inventory

    def __post_init__(self):
        self.max_armour = self.armour
        self.max_hull = self.hull
        self.max_struct = self.struct

    def take_damage(self, damage):
        if self.armour >= damage:
            self.armour -= damage
        else:
            damage -= self.armour
            self.armour = 0
            if self.hull >= damage:
                self.hull -= damage
            else:
                damage -= self.hull
                self.hull = 0
                if self.struct >= damage:
                    self.struct -= damage
                else:
                    self.struct = 0

    def is_alive(self):
        return self.struct > 0

    def attack(self, target):
        messages = [""]
        for weapon in self.weapons:
            messages.append(
                f"{self.name} fires {weapon.name} at {target.name}."
            )
            [messages.append(msg) for msg in weapon.attack(self.skill, target)]

        return messages


