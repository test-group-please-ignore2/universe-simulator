from dataclasses import dataclass
import pandas as pd
import random


@dataclass
class Settlement:
    """
    This class defines a settlement, inhabited or otherwise.
    """
    name: str = ""
    population: int = 0


settlement_question = """
How many settlements would you like to generate?
"""
num_settlements = int(input(settlement_question))
settlement_df = pd.read_csv("settlement_names.csv",
                            encoding='unicode_escape')
settlement_name_list = settlement_df["Name"].tolist()
settlement_names = random.sample(settlement_name_list, num_settlements)


settlements = []
for n in range(num_settlements):
    name = settlement_names[n]
    pop = random.randint(1000, 1000000000)
    pop_str = f"{pop:,}"
    new_settlement = Settlement(name, pop_str)
    settlements.append(new_settlement)

print(settlements)