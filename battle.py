# python libraries
from random import random
from dataclasses import dataclass, field
# my libraries
from typing import Tuple
from ships import Ship


bar = ""


@dataclass
class Battle:
    player_ship: Ship
    target_ship: Ship
    turn = 0
    over: bool = False
    messages: list = field(default_factory=list)
    results: list = field(default_factory=list)

    def __add_messages(self, *args: str) -> None:
        for arg in args:
            self.messages.append(arg)

    def __add_results(self, *args: list[Ship]) -> None:
        """
        Adds a result to the .

        Parameters:
        -----------
            None

        Returns:
        --------
            None

        """
        for arg in args:
            self.results.append(arg)

    def __end_turn(self) -> Tuple[bool, list, list]:
        """
        Describe the thing here.

        Parameters:
        -----------
            None

        Returns:
        --------
            None

        """
        return self.over, self.messages, self.results

    def __end_battle(self, msg: str, results: list) -> Tuple[bool, list, list]:
        """
        End the thing.

        Parameters:
        -----------
            msg : str
                A message to submit to close the game with.

            results : list


        Returns:
        --------
            None

        """
        self.__add_messages(msg)
        self.__add_results(results)
        self.over = True

        return self.over, self.messages, self.results

    def __check_alive(self, ship: Ship) -> None:
        """
        Check if ship is alive and end game if not.

        Parameters:
        -----------
            ship : Ship
                ship.

        Returns:
        --------
            None

        """

        if not ship.is_alive():
            self.__end_battle(
                f"{ship.name} was destroyed!",
                [ship, ship]
            )

    def __check_attacks(self) -> None:
        """
        Process an attack for the player and target, checking if they are alive
        first.

        Parameters:
        -----------
            None

        Returns:
        --------
            None

        """
        self.__add_messages(f"Turn {self.turn}:")
        attacks = self.player_ship.attack(self.target_ship)
        [self.__add_messages(msg) for msg in attacks]
        self.__check_alive(self.target_ship)
        attacks = self.target_ship.attack(self.player_ship)
        [self.__add_messages(msg) for msg in attacks]
        self.__check_alive(self.player_ship)
        self.__add_results([self.player_ship, self.target_ship])

    def play_turn(self):
        self.messages = []
        self.turn += 1
        self.__check_attacks()
        self.__add_messages(bar)
        end = self.__end_turn()

        return end
