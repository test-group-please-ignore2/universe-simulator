# ui libraries
from PyQt5 import uic
from PyQt5.QtWidgets import QAction, QMainWindow, QMdiSubWindow, QMenuBar, QSizePolicy, QTextEdit

# py libraries
import os

# my libraries
from battle import Battle
from ships import Ship
from components import Weapon
from procgen import GenerateGalaxy
from graphics import GalaxyMap, SystemMap
from misc import add_list_to_QTable
from graphics import SystemMap

from windows import GalaxyMapWindow, NewGameWindow, SystemMapWindow

from datetime import datetime

# set up environment stuff
env = os.path.dirname(os.path.abspath(__file__))
ui_file = env + "/cool.ui"


class Ui(QMainWindow):
    """
    This is the main window.

    Parameters:
    -----------
        None

    """

    def __init__(self) -> None:
        super(Ui, self).__init__()

        # load the ui file
        self.ui = uic.loadUi(ui_file, self)

        # create menu bar
        self.menuBar = QMenuBar()
        self.setMenuBar(self.menuBar)

        # game menu
        self.menuGame = self.menuBar.addMenu('Game')
        self.newGame = QAction('New Game', self)
        self.menuGame.addAction(self.newGame)

        # maps menu
        self.menuMaps = self.menuBar.addMenu('Maps')
        self.openGalaxyMap = QAction('Galaxy Map', self)
        self.menuMaps.addAction(self.openGalaxyMap)
        self.openSystemMap = QAction('System Map', self)
        self.menuMaps.addAction(self.openSystemMap)

        # load the system map
        self.systemMap = SystemMap()
        self.systemMapLayout.insertWidget(0, self.systemMap)
        size_policy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        self.systemMap.setSizePolicy(size_policy)

        # # load the galaxy map
        # self.galaxyMap = GalaxyMap()
        # self.galaxyMapLayout.insertWidget(0, self.galaxyMap)
        # size_policy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        # self.galaxyMap.setSizePolicy(size_policy)

        # empty galaxy bucket
        self.galaxy = []

        # list connections
        self.starList.currentItemChanged.connect(self.change_star)
        self.planetList.currentItemChanged.connect(self.change_planet)
        # self.starList.currentItemChanged.connect(self.generate_system_map)
        # self.planetList.currentItemChanged.connect(self.generate_system_map)
        # self.moonList.currentItemChanged.connect(self.change_moon)

        # generate button connection
        self.generateButton.clicked.connect(self.generate_galaxy)

        # # galaxy map option connections
        # self.filterHab.stateChanged.connect(self.filter_maps)
        # self.filterColour.currentIndexChanged.connect(self.filter_maps)

        # battle buttons
        self.newBattle.clicked.connect(self.new_battle)
        self.nextTurn.clicked.connect(self.next_turn)

        """
            Initialize Windows
        """
        # new game window
        self.newGameWindow = NewGameWindow(self)
        self.mainMdiArea.addSubWindow(self.newGameWindow)


        # galaxy map
        self.galaxyMapWindow = GalaxyMapWindow(self)
        self.galaxyMap = self.galaxyMapWindow.galaxyMap
        self.mainMdiArea.addSubWindow(self.galaxyMapWindow)

        # system map
        self.systemMapWindow = SystemMapWindow(self)
        self.systemMap = self.systemMapWindow.systemMap
        self.mainMdiArea.addSubWindow(self.systemMapWindow)

    def generate_galaxy(self) -> None:
        """
        This function initializes the user input variables and initiates the
        galaxy generation loop.

        Parameters:
        -----------
            None

        Returns:
        --------
            None

        """

        # clear text console
        self.textConsole.clear()

        # get rid of old lists
        self.starList.clear()
        self.planetList.clear()
        # self.moonList.clear()

        # set max values from inputs
        max_consts = self.maxConstellations.value()
        max_stars = self.maxStars.value()
        max_planets = self.maxPlanets.value()
        max_moons = self.maxMoons.value()
        map_size = self.mapSize.value()

        # run thread
        self.GenerateGalaxyThread = GenerateGalaxy(
            max_consts, max_stars, max_planets, max_moons, map_size)
        self.GenerateGalaxyThread.start()
        self.GenerateGalaxyThread.msg_sig.connect(self.write)
        self.GenerateGalaxyThread.output_sig.connect(self.set_galaxy)

    def set_galaxy(self, galaxy: list) -> None:
        """
        This function sets the current galaxy and populates the starList. Could
        be useful when saving/loading galaxys.

        Parameters:
        -----------
            galaxy : list
                A list of Constellations that makes up the galaxy.

        Returns:
        --------
            None

        """

        self.galaxy = galaxy

        # empty buckets for stars
        self.stars = []

        # add stars to star list
        self.starList.clear()
        for const in self.galaxy:
            for star in const.stars:
                self.stars.append(star)
                self.starList.addItem(star.name)

        # set selected star to the first in the list
        # self.starList.setCurrentRow(0)
        # set current star to the first in the list
        self.currentStar = self.stars[0]

        # add current star's planets to planet list
        self.planetList.clear()
        for planet in self.currentStar.orbitals:
            if planet.attr['type'] == "Planet":
                self.planetList.addItem(planet.name)

        # set selected planet to 0 so it doesn't break shit
        # self.planetList.setCurrentRow(0)
        # set current planet to the first in the list
        self.currentPlanet = None

        # update galaxy and galaxy map
        self.generate_galaxy_map()
        self.generate_system_map()

    def generate_galaxy_map(self) -> None:
        """
        This function generates and displays the galaxy map.

        TODO: Replace all of this with OpenGL stuff.

        Parameters:
        -----------
            None

        Returns:
        --------
            None

        """

        self.galaxyMap.galaxy = self.galaxy  # type: ignore
        self.galaxyMap.stars = self.stars  # type: ignore
        self.galaxyMap.update()

    def generate_system_map(self) -> None:
        """
        Gets the current selected system and draws a map of it.

        Parameters:
        -----------
            None

        Returns:
        --------
            None

        """

        if self.currentStar is not None:
            self.systemMap.set_star(self.currentStar)

        if self.currentPlanet is not None:
            self.systemMap.set_planet(self.currentPlanet)

        self.systemMap.update()

    def change_star(self) -> None:
        """
        When the star is changed in the star list, update everything else
        that needs updating.

        Parameters:
        -----------
            None

        Returns:
        --------
            None

        """
        # get remove current planet and turn off connection
        self.currentPlanet = None
        self.planetList.currentItemChanged.disconnect(self.change_planet)

        # get current star and update stuff
        row = self.starList.currentRow()
        currentStar = self.stars[row]

        # Only call generate_system_map if the current star has changed
        if currentStar != self.currentStar:
            self.currentStar = currentStar
            # show planets of star
            self.planetList.clear()
            for planet in self.currentStar.orbitals:
                if planet.attr['type'] == "Planet":
                    self.planetList.addItem(planet.name)

            # empty bucket for planet info lines
            starInfoTable = []

            # iterate over read attributes and insert into bucket
            for key, value in self.currentStar.read_attr.items():
                line = [f"{key.title()}:", value]
                starInfoTable.append(line)

            # put star info list into table
            add_list_to_QTable(self.currentStarInfo, starInfoTable)

            # Call generate_system_map to update the model view
            self.generate_system_map()

        # After updating the planet list, reconnect the change_planet function.
        self.planetList.currentItemChanged.connect(self.change_planet)

    def change_planet(self) -> None:
        """
        This function changes the currently selected planet, updating the moon
        lists and planet information.

        Parameters:
        -----------
            None

        Returns:
        --------
            None

        """
        # get currently selected planet and update stuff
        row = self.planetList.currentRow()
        currentPlanet = self.currentStar.orbitals[row]

        # Only call generate_system_map if the current planet has changed
        if currentPlanet != self.currentPlanet:
            self.currentPlanet = currentPlanet

            # empty bucket for planet info lines
            planetInfoTable = []

            # iterate over read attributes and insert into bucket
            for key, value in self.currentPlanet.read_attr.items():
                line = [f"{key.title()}:", value]
                planetInfoTable.append(line)

            # put planet info list into table
            add_list_to_QTable(self.currentPlanetInfo, planetInfoTable)

            # Call generate_system_map to update the model view
            self.generate_system_map()

    def write(self, *args) -> None:
        """
        This function writes the contents of msg_sig to the console when it is
        triggered.

        Parameters:
        -----------
            None

        Returns:
        --------
            None

        """

        # set timestamp
        timestamp = datetime.now().strftime("%Y/%m/%d %H:%M:%S")
        text = ""

        # print the text
        for arg in args:
            text = text + arg
            msg = timestamp + " >> " + text
            self.textConsole.append(msg)

    def new_battle(self) -> None:
        """
        TODO: Actually do battle stuff.

        Parameters:
        -----------
            None

        Returns:
        --------
            None

        """

        # create weapons
        laser = Weapon(
            name="Laser",
            damage=10,
            shots=1,
            accuracy=75
        )
        cannon = Weapon(
            name="Cannon",
            damage=5,
            shots=3,
            accuracy=50
        )
        missile = Weapon(
            name="Missiles",
            damage=1,
            shots=10,
            accuracy=25
        )

        player_weapons = [laser, laser, laser]
        target_weapons = [cannon, cannon, cannon]

        name = self.attackerName.text() \
            if self.attackerName.text() != "" else "Attacker"

        player_ship = Ship(
            name,
            armour=100,
            hull=100,
            struct=100,
            skill=self.attackerSkill.value(),
            weapons=player_weapons
        )

        name = self.targetName.text() \
            if self.targetName.text() != "" else "Target"

        target_ship = Ship(
            name,
            armour=100,
            hull=100,
            struct=100,
            skill=self.targetSkill.value(),
            weapons=target_weapons
        )

        self.battleLog.clear()
        self.game = Battle(player_ship, target_ship)
        self.battleLog.append(
            "A new battle has started!\n" +
            f"Attacker: {player_ship.name}\n" +
            f"Target:   {target_ship.name}\n"
        )
        self.battle_over = False

    def next_turn(self) -> None:
        """
        Next turn button stuff.

        Parameters:
        -----------
            None

        Returns:
        --------
            None

        """
        if not self.battle_over:
            over, messages, results = self.game.play_turn()
            self.battle_over = over
            player_ship = results[0][0]
            target_ship = results[0][1]

            self.attackerArmour.setValue(
                player_ship.armour /
                player_ship.max_armour *
                100
            )
            self.targetArmour.setValue(
                target_ship.armour /
                target_ship.max_armour *
                100
            )
            self.attackerHull.setValue(
                player_ship.hull /
                player_ship.max_hull *
                100
            )
            self.targetHull.setValue(
                target_ship.hull /
                target_ship.max_hull *
                100
            )
            self.attackerStruct.setValue(
                player_ship.struct /
                player_ship.max_struct *
                100
            )
            self.targetStruct.setValue(
                target_ship.struct /
                target_ship.max_struct *
                100
            )

            labels = ['Weapon', 'Damage', 'Shots', 'Accuracy']
            self.attackerWeapons.setHorizontalHeaderLabels(labels)
            self.targetWeapons.setHorizontalHeaderLabels(labels)
            self.attackerWeapons.resizeColumnsToContents()
            self.targetWeapons.resizeColumnsToContents()
            [self.battleLog.append(msg) for msg in messages]
